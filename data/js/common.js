function GetUniqueId(aLen) {
  var rnd = Math.floor(Math.random() * 26) + Date.now() 
  return aLen.toString() + rnd.toString();
};

function SanitizeFormData(frmId)
{
  // #1 - text
  $(frmId).find(':input[type=text]').each(function() {  
    this.value = removeDiacritics(this.value);  //str-utils.js
  });
  
  // #2 - password
  $(frmId).find(':input[type=password]').each(function() {  
    this.value = removeDiacritics(this.value); //str-utils.js
  });
}

function SerializeFormData(frmObj)
{
  // serialize the form data
  var data = "";
  
  // #0 - hidden inputs
  $(frmObj).find(':input[type=hidden]').each(function() {
    if (data != "") {
      data = data + "&";
    }
    data = data + this.name + "=" + encodeURIComponent(this.value);
  });  
  
  // #1 - text
  $(frmObj).find(':input[type=text]').each(function() {  
    if (data != "") {
      data = data + "&";
    }
    data = data + this.name + "=" + encodeURIComponent(this.value);
  });
    
  // #2 - combo
  $(frmObj).find('select').each(function() {
    data = data + "&" + $(this).attr("name") + "=" + encodeURIComponent($(this).val());
  });
    
  // #3 - checkboxes
  $(frmObj).find(':input[type=checkbox]').each(function() {
    data = data + "&" + this.name + "=" + (this.checked ? "1" : "0");
  });  
  
  // #4 - number
  $(frmObj).find(':input[type=number]').each(function() {
    if (data != "") {
      data = data + "&";
    }
    data = data + this.name + "=" + encodeURIComponent(this.value);
  });  
  
  // #5 - password
  $(frmObj).find(':input[type=password]').each(function() {  
    if (data != "") {
      data = data + "&";
    }
    data = data + this.name + "=" + encodeURIComponent(this.value);
  });  
  
  // #6 - date
  $(frmObj).find(':input[type=date]').each(function() {  
    if (data != "") {
      data = data + "&";
    }
    data = data + this.name + "=" + encodeURIComponent(this.value);
  });
  
  // #7 - time
  $(frmObj).find(':input[type=time]').each(function() {  
    if (data != "") {
      data = data + "&";
    }
    data = data + this.name + "=" + encodeURIComponent(this.value);
  });
  
  return data;
}

function loadActionButtons()
{
  $.ajax({ 
    type: 'GET', 
    url: 'api/status/buttons',
    timeout: 5000,  
    dataType: 'json'
  })
  .done(function (data) {    
    $.each(data, function(index, par) {
      if (par.btn_start == 0)
      {
        $("#btn-start").addClass("pure-button-disabled");  
      }
      else
      {
        $("#btn-start").removeClass("pure-button-disabled");        
      }
      
      if (par.btn_stop == 0)
      {
        $("#btn-stop").addClass("pure-button-disabled");
      }
      else
      {
        $("#btn-stop").removeClass("pure-button-disabled");
      }              
    });
  })
  .fail(function () {
    // if fail, set default data           
    var data = [
      {btn_start:"0", btn_stop:"0"},                  
    ];
                         
    $.each(data, function(index, par) {
      if (par.btn_start == 0)
      {
        $("#btn-start").addClass("pure-button-disabled");  
      }
      else
      {
        $("#btn-start").removeClass("pure-button-disabled");        
      }
      
      if (par.btn_stop == 0)
      {
        $("#btn-stop").addClass("pure-button-disabled");
      }
      else
      {
        $("#btn-stop").removeClass("pure-button-disabled");
      }                  
    });        
});   
}

function Msg(elemId, msgTxt, msgClass)
{
  var elem = "";
  var msgIdx = -1; 
  
  if (typeof elemId == "undefined")
  {
    elem = "body"
  }
  else
  {
    elem = "#" + elemId;
  }  
  msgIdx = GetUniqueId($(elem).length); 
  
  $(elem).append(
      '<div id="m-row-' + msgIdx + '" class="msg-containter" style="display: none;">'
    + ' <div class="msg-row">'
    + '  <div class="' + msgClass + '">'
    +      msgTxt + '<a id="m-close-' + msgIdx + '" class="msg-close-' + msgIdx + '" href="#">X</a>'
    + '  </div>'
    + ' <div>'
    + '</div>'
  );
  
  $("#m-close-" + msgIdx).click(function() {
    $("#m-row-" + msgIdx).fadeOut('fast', function() {
      $("#m-row-" + msgIdx).remove();
    });        
  });  
  $("#m-row-" + msgIdx).fadeIn("slow");
  /*
  //remove it after some delay
  $("#m-row-" + msgIdx).delay(3000).fadeOut('fast', function() {
    $("#m-row-" + msgIdx).remove();
  });
  */  
}

function msgWarning(msgTxt, elemId) {
  Msg(elemId, msgTxt, "msg-warning");         
}

function msgError(msgTxt, elemId) {
  Msg(elemId, msgTxt, "msg-error");         
}

function msgSucces(msgTxt, elemId) {  
  Msg(elemId, msgTxt, "msg-success");         
}

function isJSON(something) {
  if (typeof something != 'string')
  {
    something = JSON.stringify(something);
  }
  
  try {
    JSON.parse(something);
    return true;
  } catch (e) {
    return false;
  }
}

$(document).ready(function() {
  loadActionButtons();    
});