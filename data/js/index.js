function getBattery()
{
  //alert('battery');  
  $.ajax({ 
    type: 'GET', 
    url: 'api/status/voltage',
    timeout: 5000,  
    dataType: 'json'//,
    //data: { action : "getBatteryVoltage" }
  })
  .done(function (data) {
    //only one value is expected, but "each" can be used 
    $.each(data, function(index, par) {
      $("#status-battery").html(par.voltage);        
    });
  })
  .fail(function () {
      var data = [
        {voltage:"?"}
      ];
      $.each(data, function(index, par) {
        $("#status-battery").html(par.voltage);        
      });        
  });
  // ---
  setTimeout(function(){ getBattery() }, 60000);  //60s
}

function getHumidity()
{
  //alert('humidity');
  $.ajax({ 
    type: 'GET', 
    url: 'api/status/humidity',
    timeout: 5000,  
    dataType: 'json'//,
    //data: { action : "getHumidity" }
  })
  .done(function (data) {
    //only one value is expected, but "each" can be used 
    $.each(data, function(index, par) {
      $("#status-humidity").html(par.humidity);        
    });
  })
  .fail(function () {
      var data = [
        {humidity:"?"}
      ];
      $.each(data, function(index, par) {
        $("#status-humidity").html(par.humidity);        
      });        
  });  
  // ---
  setTimeout(function(){ getHumidity() }, 60000); //60s
}

function getTemperature()
{
  //alert('temperature');
  $.ajax({ 
    type: 'GET', 
    url: 'api/status/temperature',
    timeout: 5000,  
    dataType: 'json' //,
    //data: { action : "getTemperature" }
  })
  .done(function (data) {
    //only one value is expected, but "each" can be used 
    $.each(data, function(index, par) {
      $("#status-temperature").html(par.temperature);        
    });
  })
  .fail(function () {
      var data = [
        {temperature:"?"}
      ];
      $.each(data, function(index, par) {
        $("#status-temperature").html(par.temperature);        
      });        
  });  
  // ---
  setTimeout(function(){ getTemperature() }, 60000);  //60 s
}

function getActiveTask()
{
  //alert('active task');
  $.ajax({ 
    type: 'GET', 
    url: 'api/status/task',
    timeout: 5000,  
    dataType: 'json'//,
    //data: { action : "getActiveTask" }    
  })
  .done(function (data) { 
    $.each(data, function(index, par) {
      $("#actual-task-name").html(par.name + "(" + par.frequency + " Hz)");
      $("#actual-task-description").html(par.description);        
    });
  })
  .fail(function () {
      var data = [
        {name:"?", description: "?", frequency: "?"}
      ];
      $.each(data, function(index, par) {
        $("#actual-task-name").html(par.name + "(" + par.frequency + ")");
        $("#actual-task-description").html(par.description);        
      });        
  });  
  // ---
  setTimeout(function(){ getActiveTask() }, 5000);  //5s
}

function getSystemDateTime()
{
  //alert('system date time');
  $.ajax({ 
    type: 'GET', 
    url: 'api/settings/system/get',
    timeout: 5000,  
    dataType: 'json'    
  })
  .done(function (data) {
    //only one value is expected, but "each" can be used 
    $.each(data, function(index, par) {
      $("#vaporizer-date").text(par.sys_date);        
      $("#vaporizer-time").text(par.sys_time);               
    });
  })
  .fail(function () {
    //fail
    //$("#vaporizer-date").text("23.02.1984");        
    //$("#vaporizer-time").text("03:15:01");
    $("#vaporizer-date").text("?");        
    $("#vaporizer-time").text("?");               
  });
  // ---
  setTimeout(function(){ updateSystemDateTime() }, 1000);  //1s   
}

function updateSystemDateTime()
{
  //alert("updateSystemDateTime");

  var strDate = $("#vaporizer-date").text();  //dd.mm.yyyy
  var strTime = $("#vaporizer-time").text();  //hh:mm:ss

  if ((strDate != "?") && (strTime != "?"))
  {
    var day = strDate.substr(0, 2);
    var month = strDate.substr(3, 2);
    var year = strDate.substr(6, 4);
  
    //alert(year + "-" + month + "-" + day + "T" + strTime);
    var dt = new Date(year + "-" + month + "-" + day + "T" + strTime);
    var prevMinute = dt.getMinutes();
    dt.setSeconds(dt.getSeconds() + 1);
    
    strDate = ("0" + dt.getDate()).slice(-2) + "." + ("0" + (dt.getMonth() + 1)).slice(-2) + "." + dt.getFullYear();
    strTime = ("0" + dt.getHours()).slice(-2) + ":" + ("0" + dt.getMinutes()).slice(-2) + ":" + ("0" + dt.getSeconds()).slice(-2);
    
    $("#vaporizer-date").text(strDate);
    $("#vaporizer-time").text(strTime);
    
    //once per minute read device date/time
    if (prevMinute != dt.getMinutes()) {
      getSystemDateTime();      
    } else {
      setTimeout(function(){ updateSystemDateTime() }, 1000);  //1s    
    }      
  }
  else
  {
    //try get date time after while
    setTimeout(function(){ getSystemDateTime() }, 5000);  //60s
  }      
}

$(document).ready(function() {
  getTemperature();
  getHumidity();
  getBattery();
  getActiveTask();
  getSystemDateTime();  
});