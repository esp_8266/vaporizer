function CheckTaskName() {
  var tmp, res;

  res = true;
  
  tmp = $.trim( $("#task-name").val() );
  if (tmp === '')
  {
    alert("Name must be defined!");
    res = false;  
  }
  
  return res;
}

function CheckFreqSettings() {
  var tmp, tmp1, res;
  
  res = true;  
  tmp = $.trim( $("#freq-start").val() );
  tmp1 = $.trim( $("#freq-stop").val() );  
  
  if (!($.isNumeric(tmp)))
  {
    alert("Start frequency must be the number!");
    res = false;    
  }
  else
  {
    if (!($.isNumeric(tmp1)))
    {
      alert("Stop frequency must be the number!");
      res = false;    
    }
    else
    {
      var intStart = parseInt(tmp); 
      var intStop = parseInt(tmp1);     
      if (intStart > intStop)
      {
        alert("Stop frequency must be the same or greater then start frequency!");
        res = false;        
      }   
    }
  }
  
  return res;
}

$("#task-form").submit(function () {
  var res;
  
  res = CheckTaskName()
    && CheckFreqSettings();  
  
  return res;                
});

function loadTaskImgList()
{
  $("task-img").empty();
  
  $.ajax({ 
    type: 'GET', 
    url: 'api/task/img/list',
    timeout: 5000,  
    dataType: 'json'//,
    //data: { action : "getSettings" }
  })
  .done(function (data) {
    //only one value is expected, but "each" can be used 
    $.each(data, function(index, par) {
      $("#task-img").append("<option value=\"" + par.val + "\">" + par.val + "</option>");        
    });
  })
  .fail(function () {
      // if fail, set default data           
      var data = [
        {val:"default.jpg"},
        {val:"bat.jpg"},        
        {val:"deer.jpg"},
        {val:"dog.jpg"},      
        {val:"forest.jpg"},    
        {val:"human.jpg"},        
        {val:"marten.jpg"},
        {val:"mite.jpg"},        
        {val:"pig.jpg"},
        {val:"roedeer.jpg"},
        {val:"sheep.jpg"}
      ];              
      $.each(data, function(index, par) {        
        $("#task-img").append("<option value=\"" + par.val + "\">" + par.val + "</option>");
      });        
  });  
}

function loadTaskSettings()
{
  var uid = $("#task-uid").val();
  
  $.ajax({ 
    type: 'POST', 
    url: 'api/settings/task/get',
    timeout: 5000,  
    dataType: 'json',
    data: { task_uid : uid }
  })
  .done(function (data) {
    //only one value is expected, but "each" can be used 
    $.each(data, function(index, par) {
      $("#task-active").prop('checked', (par.task_active == 1)); OnChagneTaskActive();
      $("#task-name").val(par.task_name);
      $("#task-desc").val(par.task_desc);
      $("#task-img").val(par.task_img); OnChangeTaskImg();
      $("#freq-start").val(par.freq_start);
      $("#freq-stop").val(par.freq_stop);
      $("#task-pulse").val(par.task_pulse);      
      $("#task-duration").val(par.task_duration);
      $("#task-pause").val(par.task_pause);              
      // --
      $("#sched-0").prop('checked', (par.sched_0 == 1));        
      $("#sched-1").prop('checked', (par.sched_1 == 1));
      $("#sched-2").prop('checked', (par.sched_2 == 1));
      $("#sched-3").prop('checked', (par.sched_3 == 1));
      $("#sched-4").prop('checked', (par.sched_4 == 1));
      $("#sched-5").prop('checked', (par.sched_5 == 1));
      $("#sched-6").prop('checked', (par.sched_6 == 1));
      $("#sched-7").prop('checked', (par.sched_7 == 1));
      $("#sched-8").prop('checked', (par.sched_8 == 1));
      $("#sched-9").prop('checked', (par.sched_9 == 1));
      $("#sched-10").prop('checked', (par.sched_10 == 1));
      $("#sched-11").prop('checked', (par.sched_11 == 1));
      $("#sched-12").prop('checked', (par.sched_12 == 1));
      $("#sched-13").prop('checked', (par.sched_13 == 1));
      $("#sched-14").prop('checked', (par.sched_14 == 1));
      $("#sched-15").prop('checked', (par.sched_15 == 1));
      $("#sched-16").prop('checked', (par.sched_16 == 1));
      $("#sched-17").prop('checked', (par.sched_17 == 1));
      $("#sched-18").prop('checked', (par.sched_18 == 1));
      $("#sched-19").prop('checked', (par.sched_19 == 1));
      $("#sched-20").prop('checked', (par.sched_20 == 1));
      $("#sched-21").prop('checked', (par.sched_21 == 1));
      $("#sched-22").prop('checked', (par.sched_22 == 1));
      $("#sched-23").prop('checked', (par.sched_23 == 1));        
    });
  })
  .fail(function () {
      // if fail, set default data           
      var data = [
        {task_active:"1", task_name:"Name", task_desc:"Description", task_img:"default.jpg", freq_start:"25000", freq_stop:"31000", task_pulse:"0", task_duration:"60", task_pause:"0", 
         sched_0:"1", sched_1:"1", sched_2:"1", sched_3:"1", sched_4:"1", sched_5:"1", sched_6:"1", sched_7:"1", sched_8:"1", sched_9:"1", sched_10:"1", sched_11:"1",
         sched_12:"1", sched_13:"1", sched_14:"1", sched_15:"1", sched_16:"1", sched_17:"1", sched_18:"1", sched_19:"1", sched_20:"1", sched_21:"1", sched_22:"1", sched_23:"1"
        }
      ];              
      $.each(data, function(index, par) {
        $("#task-active").prop('checked', (par.task_active == 1)); OnChagneTaskActive();
        $("#task-name").val(par.task_name);
        $("#task-desc").val(par.task_desc);
        $("#task-img").val(par.task_img); OnChangeTaskImg();
        $("#freq-start").val(par.freq_start);
        $("#freq-stop").val(par.freq_stop);
        $("#task-pulse").val(par.task_pulse);        
        $("#task-duration").val(par.task_duration);
        $("#task-pause").val(par.task_pause);                        
        // --
        $("#sched-0").prop('checked', (par.sched_0 == 1));                                      
        $("#sched-1").prop('checked', (par.sched_1 == 1));
        $("#sched-2").prop('checked', (par.sched_2 == 1));
        $("#sched-3").prop('checked', (par.sched_3 == 1));
        $("#sched-4").prop('checked', (par.sched_4 == 1));
        $("#sched-5").prop('checked', (par.sched_5 == 1));
        $("#sched-6").prop('checked', (par.sched_6 == 1));
        $("#sched-7").prop('checked', (par.sched_7 == 1));
        $("#sched-8").prop('checked', (par.sched_8 == 1));
        $("#sched-9").prop('checked', (par.sched_9 == 1));
        $("#sched-10").prop('checked', (par.sched_10 == 1));
        $("#sched-11").prop('checked', (par.sched_11 == 1));
        $("#sched-12").prop('checked', (par.sched_12 == 1));
        $("#sched-13").prop('checked', (par.sched_13 == 1));
        $("#sched-14").prop('checked', (par.sched_14 == 1));
        $("#sched-15").prop('checked', (par.sched_15 == 1));
        $("#sched-16").prop('checked', (par.sched_16 == 1));
        $("#sched-17").prop('checked', (par.sched_17 == 1));
        $("#sched-18").prop('checked', (par.sched_18 == 1));
        $("#sched-19").prop('checked', (par.sched_19 == 1));
        $("#sched-20").prop('checked', (par.sched_20 == 1));
        $("#sched-21").prop('checked', (par.sched_21 == 1));
        $("#sched-22").prop('checked', (par.sched_22 == 1));
        $("#sched-23").prop('checked', (par.sched_23 == 1));
      });        
  });  
}

function loadData()
{
  loadTaskImgList();
  loadTaskSettings();
}

function initForm()
{
  //task settings
  $("#task-form").submit(function(e) {  
    e.preventDefault(); // avoid to execute the actual submit of the form.
     
    var frm = $(this);
    var mtd = frm.attr("method");    
    var f_url = frm.attr("action");

    SanitizeFormData(frm);

    //alert(SerializeFormData(frm));

    $.ajax({
      type: mtd,
      url: f_url,
      data: SerializeFormData(frm)
    })
    .done(function(data) {
      msgSucces("Settings saved", "msg-box");
    })
    .fail(function () {
      msgError("Error, while save settings!", "msg-box");      
    });
          
  });  
}

function OnChangeTaskImg()
{
  //alert("task-img - change=" + $(this).val());        
  if ($("#task-img").val() != "")
  {
    $("#task-img-select-preview").css("background-image", "url(img/task/" + $("#task-img").val() + ")");  
  }
}

function OnChagneTaskActive()
{
  //alert("task-active - change");
        
  if ($("#task-active").is(":checked"))
  {  
    $("#task-form :input").prop("disabled", false); //enable all
    //$("#task-img-select-preview").prop("disabled", false); //preview image enable
  }
  else
  {      
    $("#task-form :input").prop("disabled", true); //disable all
    //$("#task-img-select-preview").prop("disabled", true); //preview image disable
    $("#task-active").prop("disabled", false); //except this
    $("#btn-save").prop("disabled", false); //and save button    
  }
}

function initChangeHandlers()
{
  //init onchange handler and call it
  $("#task-img").on("change", OnChangeTaskImg);
  
  //init onchange handler and call it
  $("#task-active").on("change", OnChagneTaskActive);
}

$(document).ready(function() {  
  loadData();
  initForm();
  initChangeHandlers();       
});