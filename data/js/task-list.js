function BindTaskDeleteConfirm()
{
  $('.btn-tsk-del').click(function(){
      return confirm("Are you sure you want to delete the task?");
  })    
}

function loadTaskList()
{
  $("#task-list-container").html(""); //clear old list

  $.ajax({ 
    type: 'GET', 
    url: 'api/task/list',
    timeout: 5000,  
    dataType: 'json'
  })
  .done(function (data) {
    if (!isJSON(data)) {
      $('<p>No task deffined</p>').appendTo("#task-list-container");       
      return;
    }
    
    $.each(data, function(index, par) {
      $('<div id="task-idx-' + index + '" class="pure-g task-table' +  ((index % 2) == 0 ? ' color-odd' : ' color-even') + '"></div>').appendTo("#task-list-container");
      
      $('<div class="pure-u-1-2 pure-u-md-1-24"><img class="pure-img task-img-status" src="img/' + par.task_status_img + '"></div>').appendTo("#task-idx-" + index);
      $('<div class="pure-u-1-2 pure-u-md-3-24"><img class="pure-img task-img" src="img/task/' + par.task_img + '"></div>').appendTo("#task-idx-" + index);
      
      $('<div class="pure-u-1 pure-u-md-10-24"><span class="task-name"><h2>' + par.task_name + '</h2></span><span class="task-description">' + par.task_desc + '</span></div>').appendTo("#task-idx-" + index);
      $('<div class="pure-u-1 pure-u-md-6-24"><span class="task-freq">Frequency: <b>' + par.freq_start + ' - ' + par.freq_stop + '</b> Hz</span><span class="task-duration">Duration: <b>'
        + par.task_duration + ' / ' + par.task_pause + '</b> s</span><span class="task-pulsing">Pulsing: <b>' + par.task_pulse + '</b> Hz</span></div>').appendTo("#task-idx-" + index);          
  
      $('<div class="pure-u-1 pure-u-md-4-24"><p class="task-buttons">'
        + '<a class="pure-button btn-main button-small" href="settings_task.html?task_uid=' + par.uid + '"><img class="btn-img" src="img/btn-task-edit.svg"></a>'
        + '<a class="pure-button btn-main button-small btn-tsk-del" href="api/settings/task/delete?task_uid=' + par.uid + '"><img class="btn-img" src="img/btn-task-delete.svg"></a>' 
        + '</p></div>').appendTo("#task-idx-" + index);              
    });  
  })
  .fail(function () {      
      $('<p>No task deffined</p>').appendTo("#task-list-container");       
      return;
    
      // if fail, set default data - TEST           
      var data = [
        {uid:"marten.def", task_status_img:"stat-in-progress.svg", task_img:"marten.jpg", task_name:"Marten", task_desc:"Vaporize all the martens around",
        freq_start:"25000", freq_stop:"31000", task_duration:"60", task_pause:"0", task_pulse:"5"},
        
        {uid:"forest.def", task_status_img:"stat-disabled.svg", task_img:"forest.jpg", task_name:"Forest", task_desc:"Vaporize all forest animals around ... asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf",
        freq_start:"9000", freq_stop:"10000", task_duration:"30", task_pause:"5", task_pulse:"3"},
         
        {uid:"human.def", task_status_img:"stat-enabled.svg", task_img:"human.jpg", task_name:"Human", task_desc:"Vaporize all humast around ... asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf",
        freq_start:"19000", freq_stop:"21000", task_duration:"20", task_pause:"10", task_pulse:"2"},                  
      ];
                           
      $.each(data, function(index, par) {
        $('<div id="task-idx-' + index + '" class="pure-g task-table' +  ((index % 2) == 0 ? ' color-odd' : ' color-even') + '"></div>').appendTo("#task-list-container");
        
        $('<div class="pure-u-1-2 pure-u-md-1-24"><img class="pure-img task-img-status" src="img/' + par.task_status_img + '"></div>').appendTo("#task-idx-" + index);
        $('<div class="pure-u-1-2 pure-u-md-3-24"><img class="pure-img task-img" src="img/task/' + par.task_img + '"></div>').appendTo("#task-idx-" + index);
        
        $('<div class="pure-u-1 pure-u-md-10-24"><span class="task-name"><h2>' + par.task_name + '</h2></span><span class="task-description">' + par.task_desc + '</span></div>').appendTo("#task-idx-" + index);
        $('<div class="pure-u-1 pure-u-md-6-24"><span class="task-freq">Frequency: <b>' + par.freq_start + ' - ' + par.freq_stop + '</b> Hz</span><span class="task-duration">Duration: <b>'
          + par.task_duration + ' / ' + par.task_pause + '</b> s</span><span class="task-pulsing">Pulsing: <b>' + par.task_pulse + '</b> Hz</span></div>').appendTo("#task-idx-" + index);          

        $('<div class="pure-u-1 pure-u-md-4-24"><p class="task-buttons">'
          + '<a class="pure-button btn-main button-small" href="settings_task.html?task_uid=' + par.uid + '"><img class="btn-img" src="img/btn-task-edit.svg"></a>'
          + '<a class="pure-button btn-main button-small btn-tsk-del" href="api/settings/task/delete?task_uid=' + par.uid + '"><img class="btn-img" src="img/btn-task-delete.svg"></a>' 
          + '</p></div>').appendTo("#task-idx-" + index);           
      });        
  })
  .always(function() {
    BindTaskDeleteConfirm();  
  });  
}

$(document).ready(function() {
  loadTaskList();          
});