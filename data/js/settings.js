function dateForPicker(strDate)
{
  var day = strDate.substr(0, 2);
  var month = strDate.substr(3, 2);
  var year = strDate.substr(6, 4);
  
  return year + "-" + month + "-" + day;    
} 

function initEvnt()
{
  $("#use-dhcp").on("change", function() {
    var isAp = ($("#esp-mode").val() == 'ap');
    $("#ip-address").prop("disabled", this.checked || isAp);
    $("#gateway").prop("disabled", this.checked || isAp);
    $("#net-mask").prop("disabled", this.checked || isAp);
    $("#ip-dns").prop("disabled", this.checked || isAp);    
  });
  
  $("#esp-mode").on("change", function() {    
    if ($("#esp-mode").val() == "ap") 
    {
      $("#use-dhcp").prop("disabled", true);
      $("#ip-address").prop("disabled", true);
      $("#gateway").prop("disabled", true);
      $("#net-mask").prop("disabled", true);
      $("#ip-dns").prop("disabled", true);  
    }
    else
    {
      $("#use-dhcp").prop("disabled", false);
      $("#use-dhcp").trigger("change"); //call checkbox event
    }    
  });
  
  var modeVal = $("#esp-mode").val(); 
  $("#esp-mode").val(modeVal).trigger("change");
}

function loadData()
{
  $("#wifi-settings :input").prop("disabled", true); //disable all inputs
  $("#system-settings :input").prop("disabled", true); //disable all inputs
  $("#security-settings :input").prop("disabled", true); //disable all inputs
      
      
  //wifi settings
  $.ajax({   
    type: 'GET', 
    url: 'api/settings/wifi/get',
    timeout: 5000,  
    dataType: 'json'//,
    //data: { action : "getSettings" }
  })
  .done(function (data) {
    //only one value is expected, but "each" can be used
    $.each(data, function(index, par) {      
      $("#ssid").val(par.ssid);
      $("#pswd").val(par.password);      
      $("#use-dhcp").prop('checked', (par.use_dhcp == 1));
      $("#ip-address").val(par.ip_address);      
      $("#gateway").val(par.ip_gateway);
      $("#net-mask").val(par.net_mask);
      $("#ip-dns").val(par.ip_dns);
      $("#esp-mode").val(par.wifi_mode).trigger("change");               
    });
  })
  .fail(function () {
      // if fail, set default data      
      var data = [
        {wifi_mode:"ap", ssid:"Ultimate vaporizer 0.1", password:"", use_dhcp:"1", ip_address:"", ip_gateway:"", net_mask:"", ip_dns: ""}
      ];
                    
      $.each(data, function(index, par) {                
        $("#ssid").val(par.ssid);
        $("#pswd").val(par.password);      
        $("#use-dhcp").prop('checked', (par.use_dhcp == 1));
        $("#ip-address").val(par.ip_address);      
        $("#gateway").val(par.ip_gateway);
        $("#net-mask").val(par.net_mask);
        $("#ip-dns").val(par.ip_dns);
        $("#esp-mode").val(par.wifi_mode).trigger("change");
      });        
  });
  
  
  //system settings
  $.ajax({ 
    type: 'GET', 
    url: 'api/settings/system/get',
    timeout: 5000,  
    dataType: 'json'//,
    //data: { action : "getSettings" }
  })
  .done(function (data) {
    //only one value is expected, but "each" can be used 
    $.each(data, function(index, par) {
      $("#sys-date").val(dateForPicker(par.sys_date));        
      $("#sys-time").val(par.sys_time);               
    });
  })
  .fail(function () {
      // if fail, set default data
      var d = new Date();          
      
      // --- DATE      
      var month = d.getMonth()+1;
      var day = d.getDate();
      // ---       
      var sDate = (('' + day).length < 2 ? '0' : '') + day + '.'
            + (('' +  month).length < 2 ? '0' : '') + month + '.'      
            + d.getFullYear();
      
        
       // --- TIME    
      var hours = d.getHours();
      var minutes = d.getMinutes();
      var seconds = d.getSeconds();      
      // ---                            
      var sTime = (('' + hours).length < 2 ? '0' : '') + hours + ':'
            + (('' +  minutes).length < 2 ? '0' : '') + minutes + ':'      
            + (('' +  seconds).length < 2 ? '0' : '') + seconds;
      
      // json data            
      var data = [
        {sys_date:sDate, sys_time:sTime}
      ];      
             
      $.each(data, function(index, par) {        
        $("#sys-date").val(dateForPicker(par.sys_date));        
        $("#sys-time").val(par.sys_time);
      });        
  });  
  
  
  //security settings
  $.ajax({ 
    type: 'GET', 
    url: 'api/settings/security/get',
    timeout: 5000,  
    dataType: 'json'//,
    //data: { action : "getSettings" }
  })
  .done(function (data) {
    //only one value is expected, but "each" can be used 
    $.each(data, function(index, par) {
      $("#usr-name-act").val(par.usr_name);                       
    });
  })
  .fail(function () {
      // if fail, set default data
      
      // json data            
      var data = [
        {usr_name:"admin"}
      ];      
             
      $.each(data, function(index, par) {        
        $("#usr-name-act").val(par.usr_name);        
      });        
  });   
  
  
  $("#wifi-settings :input").prop("disabled", false); //enable all inputs
  $("#system-settings :input").prop("disabled", false); //enable all inputs
  $("#security-settings :input").prop("disabled", false); //enable all inputs (except "actual user name and password")  
    
  $("#esp-mode").trigger("change"); //call combo event
  $("#use-dhcp").trigger("change"); //call check box event   
}

function initForm()
{
  //wifi settings
  $("#wifi-settings").submit(function(e) {  
    e.preventDefault(); // avoid to execute the actual submit of the form.
    
    var frm = $(this);
    var mtd = frm.attr("method");    
    var f_url = frm.attr("action");

    SanitizeFormData(frm);

    $.ajax({
      type: mtd,
      url: f_url,
      data: SerializeFormData(frm)
    })
    .done(function(data) {
      msgSucces("Settings saved", "msg-box");
    })
    .fail(function () {
      msgError("Error, while save settings!", "msg-box");      
    });
          
  });
  
  
  //system settings
  $("#system-settings").submit(function(e) {  
    e.preventDefault();

    var frm = $(this);
    var mtd = frm.attr("method");    
    var f_url = frm.attr("action");

    SanitizeFormData(frm);

    $.ajax({
      type: mtd,
      url: f_url,
      data: SerializeFormData(frm)
    })
    .done(function(data) {
      msgSucces("Settings saved", "msg-box");
    })
    .fail(function () {
      msgError("Error, while save settings!", "msg-box");
    });
      
  });
  
  
  //security settings
  $("#security-settings").submit(function(e) {
    e.preventDefault();
        
    //check input values
    if ($.trim($("#usr-name-act").val()).length == 0) {
      msgError("Current user name is not defined!", "msg-box");
      return false;      
    }
    
    if ($.trim($("#usr-name-new").val()).length == 0) {
      msgError("New user name is not defined!", "msg-box");
      return false;      
    }    
        
    if ($("#usr-psw-new").val() !== $("#usr-psw-confirm").val()) {          
      msgError("Passwords are different!", "msg-box");      
      return false;      
    }
  
    var frm = $(this);
    var mtd = frm.attr("method");    
    var f_url = frm.attr("action");

    SanitizeFormData(frm);

    $.ajax({
      type: mtd,
      url: f_url,
      data: SerializeFormData(frm)
    })
    .done(function(data) {
      msgSucces("Settings saved", "msg-box");
    })
    .fail(function () {
      msgError("Error, while save settings!", "msg-box");      
    });
      
  });    
}

$(document).ready(function() {
  loadData();
  initEvnt();      
  initForm();  
});