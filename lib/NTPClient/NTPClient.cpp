/**
 * The MIT License (MIT)
 * Copyright (c) 2019 by SaLIk
 */

//#define DEBUG_NTPClient 1
#define SEVENTY_YEARS_SEC 2208988800UL

#include "NTPClient.h"

NTPClient::NTPClient(UDP *udp) {
  this->_udp = udp;
}

NTPClient::NTPClient(UDP *udp, int32 timeOffset) {
  this->_udp = udp;
  this->_timeOffset = timeOffset;
}

NTPClient::NTPClient(UDP *udp, const char *poolServerName) {
  this->_udp = udp;
  this->_poolServerName = poolServerName;
}

NTPClient::NTPClient(UDP *udp, const char *poolServerName, int32 timeOffset) {
  this->_udp = udp;
  this->_timeOffset = timeOffset;
  this->_poolServerName = poolServerName;
}

NTPClient::NTPClient(UDP *udp, const char *poolServerName, int32 timeOffset, uint16 updateInterval) {
  this->_udp = udp;
  this->_timeOffset = timeOffset;
  this->_poolServerName = poolServerName;
  this->_updateInterval = updateInterval;
}

void NTPClient::begin() {
  this->begin(NTP_DEFAULT_LOCAL_PORT);
}

void NTPClient::begin(uint16 port) {
  this->_port = port;
  this->_udp->begin(this->_port);
  this->_udpSetup = true;
}

bool NTPClient::forceUpdate() {
  #ifdef DEBUG_NTPClient
    Serial.println("Update from NTP Server");
  #endif

  //send NTP request
  this->sendNTPPacket();

  // Wait till data is there or timeout...
  byte timeout = 0;
  int cb = 0;
  do {
    delay(10);
    cb = this->_udp->parsePacket();
    if (timeout > 100) return false; // timeout after 1000 ms
    timeout++;
  } while (cb == 0);
  this->_lastUpdate = millis() - (10 * (timeout + 1)); // Account for delay in reading the time

  //read response from NTP server
  this->_udp->read(this->_packetBuffer, sizeof(this->_packetBuffer));

  uint16 highWord = word(this->_packetBuffer[40], this->_packetBuffer[41]);
  uint16 lowWord = word(this->_packetBuffer[42], this->_packetBuffer[43]);
  // combine the four bytes (two words) into a long integer
  // this is NTP time (seconds since Jan 1 1900):
  _unixTimestamp = (((uint32)highWord << 16) | lowWord) - SEVENTY_YEARS_SEC;

  return this->parseTimeStamp();
}

bool NTPClient::update() {
  if (
    ((millis() - this->_lastUpdate) >= this->_updateInterval) //Update after _updateInterval
    || (this->_lastUpdate == 0) //Update if there was no update yet.
  )
  {                 
    //setup the UDP client if needed               
    if (!this->_udpSetup)
    {
      this->begin();
    }    
    return this->forceUpdate();
  }
  return true;
}

uint32 NTPClient::getUnixTime() {
  uint32 ut = this->_unixTimestamp + ((millis() - this->_lastUpdate) / 1000);
  if ( (this->_timeOffset < 0) && (abs(this->_timeOffset) > ut) )
  {
    ut = 0;
  }
  else
  {
    ut = ut + this->_timeOffset;
  }
  return ut;
}

uint8 NTPClient::getDay() {
  return this->_day;
}

uint8 NTPClient::getMonth() {
  return this->_month;   
}

uint16 NTPClient::getYear() {
  return this->_year;  
}

uint8 NTPClient::getHours() {
  return this->_hour;
}

uint8 NTPClient::getMinutes() {
  return this->_minute;
}

uint8 NTPClient::getSeconds() {
  return this->_second;
}

void NTPClient::end() {
  this->_udp->stop();

  this->_udpSetup = false;
}

void NTPClient::setTimeOffset(int timeOffset) {
  this->_timeOffset = timeOffset;
  this->parseTimeStamp(); //compute new date time
}

void NTPClient::setUpdateInterval(int updateInterval) {
  this->_updateInterval = updateInterval;
}

void NTPClient::sendNTPPacket() {
  // set all bytes in the buffer to 0
  memset(this->_packetBuffer, 0, sizeof(_packetBuffer));

  //Initialize values needed to form NTP request
  this->_packetBuffer[0] = 0b11100011;  //LI(0b11=3)=clock is unsynchronized, Version(0b100=4)=current version is 4, Mode(0b011=3)=client
  this->_packetBuffer[1] = 0;   //Stratum, or type of clock(0)=unspecified or invalid
  this->_packetBuffer[2] = 6;   //Polling Interval(6)=maximum interval between successive NTP messages
  this->_packetBuffer[3] = 0xEC;  //Peer Clock Precision(0xEC=236)=precision of the szstem clock in log2 seconds

  //Root Delay(4 BYTES)[4-7] = 0
  //Root Dispersion(4 BYTES)[8-11] = 0

  //Reference timestamp
  this->_packetBuffer[12]  = 0x1A;
  this->_packetBuffer[13]  = 0x9A;
  this->_packetBuffer[14]  = 0xC1;
  this->_packetBuffer[15]  = 0x20;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  this->_udp->beginPacket(this->_poolServerName, 123); //NTP requests are to port 123
  this->_udp->write(this->_packetBuffer, NTP_PACKET_SIZE);
  this->_udp->endPacket();
}

bool NTPClient::parseTimeStamp()
{
  bool res;    
  uint16 yearOffset;
  uint8 leapYear;
  uint8 m;
  uint8 monthDays;
  uint8 daysInMonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  //parse time
  this->_second = this->getUnixTime() % 60;
  this->_minute = (this->getUnixTime() / 60) % 60;
  this->_hour = (this->getUnixTime() / 3600) % 24;

  //parse year
  uint32 days = ((this->getUnixTime() / 3600) / 24); //days until 1.1.1970
  for (yearOffset = 0; yearOffset <= (UINT16_MAX - 1970); yearOffset++)
  {
    if ((yearOffset % 4) == 0)
    {
      leapYear = 1;
    }
    else
    {
      leapYear = 0;
    }

    if (days < ((uint32)365 + leapYear)) 
    {
      break;
    }
    else
    {
      days = days - ((uint32)365 + leapYear);
    } 
  }  
  this->_year = 1970 + yearOffset;
  
  //parse month
  for (m = 1; m <= 12; m++)
  {
    monthDays = daysInMonth[m - 1];
    if ((m == 2) && (leapYear == 1))
    {
      monthDays++;
    }
    if (days < monthDays)
    {
      break;
    }
    else
    {
      days = days - monthDays;
    }
  }
  this->_month = m;

  //parse days
  this->_day = days + 1;

  res = (yearOffset < (UINT16_MAX - 1970)); //check at least the year
  return res;
}