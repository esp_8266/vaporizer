#pragma once

#include "Arduino.h"
#include <Udp.h>

#define NTP_PACKET_SIZE 48
#define NTP_DEFAULT_LOCAL_PORT 1337


class NTPClient {
  private:
    UDP *_udp = NULL;
    const char* _poolServerName = "time.nist.gov"; //default time server
    uint16 _port = NTP_DEFAULT_LOCAL_PORT;

    bool _udpSetup = false;

    int32 _timeOffset = 0;  
    uint32 _unixTimestamp = 0;  //Unix timestamp (secoonds from 1.1.1970)
    uint32 _lastUpdate = 0; //ms
    uint16 _updateInterval = 60000;  //ms

    uint8 _hour = 0;
    uint8 _minute = 0;
    uint8 _second = 0;
    uint8 _day = 1;
    uint8 _month = 1;
    uint16 _year = 1970;

    byte _packetBuffer[NTP_PACKET_SIZE];    
    void sendNTPPacket();

    bool parseTimeStamp();

  public:
    NTPClient(UDP *udp);
    NTPClient(UDP *udp, int timeOffset);
    NTPClient(UDP *udp, const char *poolServerName);
    NTPClient(UDP *udp, const char *poolServerName, int32 timeOffset);
    NTPClient(UDP *udp, const char *poolServerName, int32 timeOffset, uint16 updateInterval);

    /**
     * Starts the underlying UDP client with the default local port
     */
    void begin();

    /**
     * Starts the underlying UDP client with the specified local port
     */
    void begin(uint16 port);

    /**
     * This should be called in the main loop of your application. By default an update from the NTP Server is only
     * made every 60 seconds. This can be configured in the NTPClient constructor.
     *
     * @return true on success, false on failure
     */
    bool update();

    /**
     * This will force the update from the NTP Server.
     *
     * @return true on success, false on failure
     */
    bool forceUpdate();

    /**
     * Changes the time offset (in seconds). Useful for changing timezones dynamically
     */
    void setTimeOffset(int timeOffset);

    /**
     * Set the update interval to another frequency. E.g. useful when the
     * timeOffset should not be set in the constructor
     */
    void setUpdateInterval(int updateInterval);

    /**
     * Stops the underlying UDP client
     */
    void end();

    /**
     * @return time in seconds since Jan. 1, 1970
     */
    uint32 getUnixTime();    

    /**
     * Date time values
     */
    uint8 getDay();
    uint8 getMonth();
    uint16 getYear();
    uint8 getHours();
    uint8 getMinutes();
    uint8 getSeconds();    
};
