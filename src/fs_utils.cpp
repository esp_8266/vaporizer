#include "fs_utils.h"
#include "wdt_utils.h"
#include "str_utils.h"
#include "vap_global.h"

t_dbg_setup in_dbgSetup = dsNone;

String getContentType(String filename)
{
  if (filename.endsWith(".htm"))
    return "text/html";
  else if (filename.endsWith(".html"))
    return "text/html";
  else if (filename.endsWith(".css"))
    return "text/css";
  else if (filename.endsWith(".js"))
    return "application/javascript";
  else if (filename.endsWith(".png"))
    return "image/png";
  else if (filename.endsWith(".gif"))
    return "image/gif";
  else if (filename.endsWith(".jpg"))
    return "image/jpeg";
  else if (filename.endsWith(".svg"))
    return "image/svg+xml";    
  else if (filename.endsWith(".ico"))
    return "image/x-icon";
  else if (filename.endsWith(".xml"))
    return "text/xml";
  else if (filename.endsWith(".pdf"))
    return "application/x-pdf";
  else if (filename.endsWith(".zip"))
    return "application/x-zip";
  else if (filename.endsWith(".gz"))
    return "application/x-gzip";
  else if (filename.endsWith(".def"))
    return ""; //forbidden file
  return "text/plain";
}

size_t FS_ReadUntil(File *f, char *row, size_t rowMaxByteLength, const char *strTerminator, t_dbg_setup dbgSetup)
{
  uint16 idx;
  size_t res;
  size_t row_size;
  char *posStr;

  idx = 0;
  row_size = 0;
  posStr = NULL;
  str_init(row, rowMaxByteLength);

  res = (*f).readBytes(&row[idx], 1);
  while (res == 1)
  {
    row_size = row_size + res;

    //check termintaor
    posStr = strstr(&row[0], strTerminator);
    if (posStr != NULL)
    {
      break; //terminator reached
    }
    else if (row_size < (rowMaxByteLength - 2)) //check row length, leave 1B in the end for string termintaor
    {
      idx++;
      res = (*f).readBytes(&row[idx], 1); //read next byte
    }
    else
    {
      break; //maximum row length, break reading
    }
  }

  //add string terminator
  if (posStr != NULL)
  {
    (*posStr) = '\0';
  }
  else
  {
    if (res == 1)
    {
      idx++;
    }
    row[idx] = '\0';
  }

  if (dbgSetup == dsUltimateDetails)
  {
    DBG_OUTPUT_PORT.printf("Readed data: \"%s\" \n", row);
  }

  wdtReset(dbgSetup);
  return strlen(row);
}

size_t FS_ReadLine_CRLF(File *f, char *row, size_t rowMaxByteLength, t_dbg_setup dbgSetup)
{
  return FS_ReadUntil(f, row, rowMaxByteLength, "\r\n", dbgSetup);
}