#include <Arduino.h>
#include "wdt_utils.h"
#include "vap_global.h"

void wdtSoftDisable(t_dbg_setup dbgSetup)
{
  if (dbgSetup == dsUltimateDetails)
  {
    if (wdt_SoftActive)
    {
      DBG_OUTPUT_PORT.printf("Software WDT disabled ... \n");
    }
  }  

  if (wdt_SoftActive)
  {
    wdt_SoftActive = false;
    wdtReset(dbgSetup);
    ESP.wdtDisable();
  }
}

void wdtSoftEnable(t_dbg_setup dbgSetup)
{
  if (dbgSetup == dsUltimateDetails)
  {
    if (!wdt_SoftActive)
    {    
      DBG_OUTPUT_PORT.printf("Software WDT enabled ... \n");
    }
  }

  if (!wdt_SoftActive)
  {
    wdt_SoftActive = true;
    wdtReset(dbgSetup);
    ESP.wdtEnable(1000);
  }
}

void wdtReset(t_dbg_setup dbgSetup)
{
  ESP.wdtFeed();
  
  if (dbgSetup == dsUltimateDetails)
  {
    DBG_OUTPUT_PORT.printf("WDT feeded ... \n");
  }
}