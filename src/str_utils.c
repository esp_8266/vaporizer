#include <ctype.h>
#include "str_utils.h"

void str_init(char *str, size_t strByteLength)
{
  memset(str, 0, strByteLength);
}

char *str_setLength(char *str, uint16 strByteLength)
{
  if (str == NULL)
  {
    if (strByteLength > 0)
    {
      return (char *)malloc(strByteLength * sizeof(char));
    }
    else
    {
      return NULL;
    }
  }
  else
  {
    if (strByteLength > 0)
    {
      return (char *)realloc(str, strByteLength * sizeof(char));
    }
    else
    {
      free(str);
      return NULL;
    }
  }
}

bool str_replace(char *str, size_t strMaxByteSize, const char *strPatern, const char *strReplace)
{
  bool res = false;

  size_t lenPatern = strlen(strPatern);
  size_t lenReplace = strlen(strReplace);
  size_t lenStr = strlen(str);

  uint16 i;
  uint16 j;
  uint16 diff;
  char *posStr;

  i = 0;
  posStr = strstr(&str[i], strPatern); //STRSTR returns a pointer to the first occurrence of "strPatern" in "str"
  while (posStr != NULL)
  {
    res = true; //at leas ONE occurence was found
    
    //search for patern position
    i = posStr - &str[i];

    /*
    for (i = 0; i < lenStr; i++)
    {
      if (&str[i] == posStr)
      {
    */

        //position of the patern founded
        //printf("Patern \"%s\" found at position %d\n", strPatern, i);

        if (lenPatern == lenReplace)
        {
          strncpy(&str[i], strReplace, lenReplace); //must use strNcpy because strLcpy termintate string after action                    
        }
        else if (lenPatern < lenReplace)
        {
          //prepare space for replace (move to RIGHT)
          diff = lenReplace - lenPatern;
          for (j = lenStr; j >= (i + lenPatern); j--) //can move str[lenStr] because here is "\0" of the str
          {
            if ((j + diff) <= (strMaxByteSize - 1))
            {
              //printf("Move char \"%c\", str[%d] -> str[%d] \n", str[j], j, j + diff);
              str[j + diff] = str[j];
            }
          }

          //printf("\nPrepared string:\n%s \n", str);

          //replace string
          if ((lenStr + diff) <= (strMaxByteSize - 1))
          {
            //replaced string fit str array
            strncpy(&str[i], strReplace, lenReplace);
            str[lenStr + diff + 1] = '\0'; //move string terminmator
          }
          else if ((i + lenReplace) < strMaxByteSize)
          {
            //replacet string does not fit str, but replacement will not be trimmed
            strncpy(&str[i], strReplace, lenReplace);
            str[strMaxByteSize - 1] = '\0'; //move string terminmator
          }
          else
          {
            //replacement will be trimmed
            strncpy(&str[i], strReplace, (strMaxByteSize - 1 - i));
            str[strMaxByteSize - 1] = '\0'; //move string terminmator
          }
        }
        else //lenPatern > lenReplace
        {
          //move part of string
          diff = lenPatern - lenReplace;
          for (j = (i + lenPatern); j <= lenStr; j++) //can move str[lenStr] because here is "\0" of the str
          {
            str[j - diff] = str[j];
          }

          //replace string
          strncpy(&str[i], strReplace, lenReplace);

          //move string terminator
          str[lenStr - diff + 1] = '\0';
        }

    /*
        break;
      }
    } //end-for
    */

    lenStr = strlen(str);                //update str length
    posStr = strstr(&str[i], strPatern); //search next patern
  }                                      //end-while

  return res;
}

  int32 str_pos(const char *str, const char *strPatern)
  {
    char *pos = NULL;
    pos = strstr(str, strPatern);
    if(pos) {
      return pos - str;
    }
    else
    {
      return -1;
    }    
  }

  void str_replace_all(char *str, size_t strMaxByteSize, const char *strPatern, const char *strReplace)
  {
    while (str_pos(str, strPatern) != -1)
    {
      str_replace(str, strMaxByteSize, strPatern, strReplace);
    }
  }

void str_sanitizeNonPrintableChars(char *str)
  {
    //remove non printable ASCII characters
    uint16 i;
    uint16 sLen = strlen(str);

    for (i=0; i < sLen; i++)
    {
      if (isprint( (unsigned char) str[i] ) == 0)
      {
        str[i] = '_'; //unprintable characters to "_"
      }
    }
  }

  void str_sanitizeHttpParam(char *str)
  {
    str_sanitizeNonPrintableChars(str);
  }

  void str_sanitizeFileName(char *str)
  {
    //nonprintable
    str_sanitizeNonPrintableChars(str);

    //and forbidden
    uint16 sLen = strlen(str);
    str_replace_all(str, sLen, "<", "");
    str_replace_all(str, sLen, ">", "");
    str_replace_all(str, sLen, ":", "");
    str_replace_all(str, sLen, "/", "");
    str_replace_all(str, sLen, "\\", "");
    str_replace_all(str, sLen, "|", "");
    str_replace_all(str, sLen, "?", "");
    str_replace_all(str, sLen, "*", "");
    str_replace_all(str, sLen, ",", "");
    str_replace_all(str, sLen, ".", "");
    str_replace_all(str, sLen, "+", "");
    str_replace_all(str, sLen, "!", "");
    str_replace_all(str, sLen, "\"", "");
    str_replace_all(str, sLen, "'", "");
    str_replace_all(str, sLen, " ", "");
    str_replace_all(str, sLen, ";", "");
    str_replace_all(str, sLen, "(", "");
    str_replace_all(str, sLen, ")", "");
    str_replace_all(str, sLen, "{", "");
    str_replace_all(str, sLen, "}", "");
    str_replace_all(str, sLen, "`", "");
    str_replace_all(str, sLen, "^", "");
  }

/*boolean charIsNumber(char *strNum)
{    
    char numbers[] = "0123456789";
    uint8 numLen;
    uint8 i;    

    numLen = strlen(numbers);
    for (i = 0; i < numLen; i++)
    {
        if (strNum == numbers[i])
        {
            return true;
        }
    }
    return false;
}*/

bool str2uint8(char *str, uint8 *num)
{
  char *ptr;

  *num = (uint8)strtoul(str, &ptr, 10);
  return (*ptr == '\0');
}

bool str2uint16(char *str, uint16 *num)
{
  char *ptr;

  *num = (uint16)strtoul(str, &ptr, 10);
  return (*ptr == '\0');
}

bool str2uint32(char *str, uint32 *num)
{
  char *ptr;

  *num = strtoul(str, &ptr, 10);
  return (*ptr == '\0');
}

bool str2float(char *str, float *num)
{
  char *ptr;

  *num = strtof(str, &ptr);
  return (*ptr == '\0');
}

bool uint16_2str(uint16 *num, char *str, uint16 strMaxByteSize)
{
  uint16 len;

  len = (uint16)snprintf(str, strMaxByteSize, "%d", *num);
  return (len > 0);
}

bool uint8_2str(uint8 *num, char *str, uint16 strMaxByteSize)
{
  uint16 num16;
  num16 = (uint16)*num;

  return uint16_2str(&num16, str, strMaxByteSize);
}

bool float2str(float *num, char *str, size_t strMaxByteSize)
{
  uint16 len;

  /* 
        %[cislo parametru][priznaky][sirka][.presnost][velikost]typ

        [cislo parametru] - kdyz je potreba zmenit poradi slov zadanych jako parametry
        [priznaky] - umoznuje zaorvnani vystupu a upraveni zobrazovani znamenek, nepouzivam
        [sirka] - minimalni pocet znaku
        [.presnost] - u cisel s carkou udava na kolik mist ma byt vypis zaokrouhlen, u textu udava maximalni pocet vypasnych znaku
        [velikost] - udava presny typ parametru, nepouzivam
        typ - konkretni typ parametru
    */

  len = (uint16)snprintf(str, strMaxByteSize, "%.2f", *num);
  return (len > 0);
}

bool float2strEx(float *num, char *str, uint8 decimalCount, size_t strByteMaxLen)
{
  uint16 len;
  char format[10] = "";
  char sUInt8[5] = "";

  /* 
        %[cislo parametru][priznaky][sirka][.presnost][velikost]typ

        [cislo parametru] - kdyz je potreba zmenit poradi slov zadanych jako parametry
        [priznaky] - umoznuje zaorvnani vystupu a upraveni zobrazovani znamenek, nepouzivam
        [sirka] - minimalni pocet znaku
        [.presnost] - u cisel s carkou udava na kolik mist ma byt vypis zaokrouhlen, u textu udava maximalni pocet vypasnych znaku
        [velikost] - udava presny typ parametru, nepouzivam
        typ - konkretni typ parametru
    */

  strlcat(&format[0], "%.", sizeof(format));
  uint8_2str(&decimalCount, &sUInt8[0], sizeof(sUInt8));
  strlcat(&format[0], &sUInt8[0], sizeof(format));
  strlcat(&format[0], "f", sizeof(format));

  len = (uint16)snprintf(str, strByteMaxLen, &format[0], *num);
  return (len > 0);
}