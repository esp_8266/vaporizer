#include "vap_global.h"
#include "str_utils.h"
#include "task.h"
//#include "wdt_utils.h"

void initTask(t_task *tsk)
{
  (*tsk).idx = 0;
  (*tsk).uid = NULL;
  (*tsk).status = tsUnknown;
  (*tsk).img = NULL;
  (*tsk).name = NULL;
  (*tsk).description = NULL;
  (*tsk).duration = 0;
  (*tsk).pause = 0;
  (*tsk).pause_counter = 0;
  (*tsk).freqPulse = 0;
  (*tsk).freqStart = 0;
  (*tsk).freqStop = 0;
  (*tsk).schedule = 0;
}

void finalizeTask(t_task *tsk)
{
  (*tsk).uid = str_setLength((*tsk).uid, 0);
  (*tsk).img = str_setLength((*tsk).img, 0);
  (*tsk).name = str_setLength((*tsk).name, 0);
  (*tsk).description = str_setLength((*tsk).description, 0);
}

void assignTaskParams(t_task *tskSrc, t_task *tskDst)
{
  //uid
  (*tskDst).uid = str_setLength((*tskDst).uid, strlen((*tskSrc).uid) + 1);
  if (strlen((*tskSrc).uid) > 0)
  {
    strlcpy((*tskDst).uid, (*tskSrc).uid, strlen((*tskSrc).uid) + 1);
  }
  else 
  {
    str_init((*tskDst).uid, strlen((*tskSrc).uid) + 1);
  }

  //name
  (*tskDst).name = str_setLength((*tskDst).name, strlen((*tskSrc).name) + 1);
  if (strlen((*tskSrc).name) > 0)
  {
    strlcpy((*tskDst).name, (*tskSrc).name, strlen((*tskSrc).name) + 1);
  }
  else
  {
    str_init((*tskDst).name, strlen((*tskSrc).name) + 1);  
  }

  //description
  (*tskDst).description = str_setLength((*tskDst).description, strlen((*tskSrc).description) + 1);
  if (strlen((*tskSrc).description) > 0)
  {
    strlcpy((*tskDst).description, (*tskSrc).description, strlen((*tskSrc).description) + 1);
  }
  else
  {
    str_init((*tskDst).description, strlen((*tskSrc).description) + 1);  
  }

  //image
  (*tskDst).img = str_setLength((*tskDst).img, strlen((*tskSrc).img) + 1);
  if (strlen((*tskSrc).img) > 0)
  {
    strlcpy((*tskDst).img, (*tskSrc).img, strlen((*tskSrc).img) + 1);
  }
  else
  {
    str_init((*tskDst).img, strlen((*tskSrc).img) + 1);
  }
  

  //idx
  (*tskDst).idx = (*tskSrc).idx;

  //status
  (*tskDst).status = (*tskSrc).status;

  //freq
  (*tskDst).freqStart = (*tskSrc).freqStart;
  (*tskDst).freqStop = (*tskSrc).freqStop;

  //pulsing
  (*tskDst).freqPulse = (*tskSrc).freqPulse;

  //duration
  (*tskDst).duration = (*tskSrc).duration;

  //pause
  (*tskDst).pause = (*tskSrc).pause;

  //pause counter
  (*tskDst).pause_counter = (*tskSrc).pause_counter;

  //scheduller
  (*tskDst).schedule = (*tskSrc).schedule;
}

bool TaskIsPaused(t_task *tsk)
{
  return (*tsk).pause_counter != 0;
}

t_task *taskListSetSize(t_task *tsk, uint8 actSize, uint8 newSize)
{
  t_task *res = NULL;
  uint8 i;

  if (tsk == NULL)
  {
    if (newSize > 0)
    {
      res = (t_task *)malloc(sizeof(t_task) * newSize);
      for (i = 0; i < newSize; i++)
      {
        initTask(&res[i]);
      }
    }
  }
  else
  {
    if (newSize > 0)
    {
      if (newSize > actSize)
      {
        res = (t_task *)realloc(tsk, sizeof(t_task) * newSize);
        for (i = actSize; i < newSize; i++)
        {
          initTask(&res[i]);
        }
      }
      else if (newSize < actSize)
      {
        for (i = actSize - 1; i >= newSize; i--)
        {
          finalizeTask(&tsk[i]); //free task memory
        }
        res = (t_task *)realloc(tsk, sizeof(t_task) * newSize);
      }
    }
    else
    {
      for (i = 0; i < actSize; i++)
      {        
        finalizeTask(&tsk[i]); //free task memory
      }

      if (actSize > 0)
      {
        free(tsk);
      }
      res = NULL;
    }
  }

  return res;
}

void setSchedule(uint32 *sched, uint8 idx, uint8 val)
{
  if (val == 1) 
  {
    *sched = *sched | (uint32)(1 << idx);
  }
  else
  {
    *sched = *sched & ~(uint32)(1 << idx);
  }          
}
