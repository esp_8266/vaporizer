#include <Arduino.h>
#include <EEPROM.h>
#include <FS.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <stdarg.h>
#include <Wire.h>
#include <RTClib.h>
#include <Ticker.h>
#include <MD_AD9833.h>
#include <SPI.h>
#include <math.h>
#include <DHT.h>
#include <WiFiUdp.h>
#include <NTPClient.h>

#include "vap_global.h"
#include "str_utils.h"
#include "fs_utils.h"
#include "wdt_utils.h"
#include "task.h"

//debug level
//t_dbg_setup dbgSetup = dsNone;
//t_dbg_setup dbgSetup = dsMinimal;
t_dbg_setup dbgSetup = dsDefault;
//t_dbg_setup dbgSetup = dsDetails;
//t_dbg_setup dbgSetup = dsUltimateDetails

t_global global;
t_settings settings;
t_system_ticker tck;
Ticker *p_ticker = NULL;
t_task *p_task_list = NULL;
ESP8266WebServer *p_srv = NULL;
RTC_DS3231 *p_rtc = NULL;
MD_AD9833 *p_ad9833 = NULL;
DHT *p_dht22 = NULL;
WiFiUDP *p_udp = NULL;
NTPClient *p_ntp = NULL;


size_t DBG_printf(t_dbg_setup dbg, const char *format, ...)
{
  bool doDbg;

  if (dbgSetup == dsUltimateDetails)
  {
    doDbg = (dbg == dsUltimateDetails) || (dbg == dsDetails) || (dbg == dsDefault);
  }
  else if (dbgSetup == dsDetails)
  {
    doDbg = (dbg == dsDetails) || (dbg == dsDefault);
  }
  else if (dbgSetup == dsDefault)
  {
    doDbg = (dbg == dsDefault) || (dbg == dsMinimal);
  }
  else if (dbgSetup == dsMinimal)
  {
    doDbg = (dbg == dsMinimal);
  }  
  else
  {
    doDbg = false;
  }

  if (doDbg)
  {
    va_list arg;    
    char *buffer = NULL;
    size_t len;

    buffer = str_setLength(buffer, 64);
    va_start(arg, format);
    len = vsnprintf(buffer, 64, format, arg);
    va_end(arg);

    if (len > (64 - 1))
    {
      buffer = str_setLength(buffer, len + 1);
      if (!buffer)
      {
        return 0;
      }
      va_start(arg, format);
      vsnprintf(buffer, len + 1, format, arg);
      va_end(arg);
    }
    len = DBG_OUTPUT_PORT.print(buffer);
    buffer = str_setLength(buffer, 0);
    return len;
  }
  else
  {
    return 0;
  }
}

void init_uart(void)
{
  //uart (TX=GPIO1, RX=GPIO3)t
  DBG_OUTPUT_PORT.begin(UART_SPEED, SERIAL_8N1, SERIAL_FULL);
}

void ClearActiveTask(void)
{  
  global.task_active_idx = 0;
  global.task_active_freq = 0;
  global.task_active_freq_step = 0;
  global.task_active_pulse_thr = 0;
  global.task_active_duration = 0;
}

// <1, 7>, for Gregorian calender
// https://en.wikipedia.org/wiki/Zeller%27s_congruence 
uint8 dayOfWeek(uint8 aDay, uint8 aMonth, uint16 aYear)
{
  uint8 res;  
  uint8 K = aYear % 100;
  uint8 J = floor(aYear / 100);
  uint8 m = aMonth;
  if (m < 3)
  {
    m = m + 12;
  }

  res = (uint16)( aDay + floor((13*(m+1))/5) + K + floor(K/4) + floor(J/4) - (2*J) ) % 7;
  res = ((res + 5) % 7) + 1; //convert to 1=monday ... 7=suday

  return res;
}

/*
 * Code for Central Europe (tested for every day in range 2014-3000 year)
 * https://stackoverflow.com/questions/5590429/calculating-daylight-saving-time-from-only-date
 */
bool isSummerTime(uint16 aYear, uint8 aMonth, uint8 aDay, uint8 aHour)
{
  bool res;
  uint8 lastDayOfWeekInMonth;
  uint8 lastSundayDay;

  //check main year parts
  if ((aMonth < 3) || (aMonth > 10))
  {
    res = false;
    return res;
  }    
  if ((aMonth > 3) && (aMonth < 10))
  {
    res = true;
    return res;
  }

  //check if it is the day after the last sunday in the month    
  if (aMonth == 3)
  {
    lastDayOfWeekInMonth = dayOfWeek(31, aMonth, aYear);
    lastSundayDay = 31 - (lastDayOfWeekInMonth % 7);
    res = (lastSundayDay == aDay) && (aHour >= 2);
    return res;
  }
  if (aMonth == 10)
  {
    lastDayOfWeekInMonth = dayOfWeek(31, aMonth, aYear);
    lastSundayDay = 31 - (lastDayOfWeekInMonth % 7);
    res = (lastSundayDay == aDay) && (aHour < 3);
    return res;
  }

  res = false; // this line never gonna happen
  return res;
}

void init_global(void)
{
  DBG_printf(dsDefault, "Init global variables ... ");

  global.temperature = -1;
  global.humidity = -1;
  global.battery_voltage = -1;
  //
  global.task_cnt = 0;
  ClearActiveTask();
  // ---
  strlcpy(global.ip, "", sizeof(global.ip));
  // ---
  global.tHour = 0;
  global.tMin = 0;
  global.tSec = 1;
  global.dDay = 1;
  global.dMonth = 1;
  global.dYear = 1900;
  
  DBG_printf(dsDefault, "OK \n");
}

void init_vap_pulseIO(uint8 mode, uint8 val)
{
  pinMode(PIN_VAP_PULSE, mode);
  if (mode == OUTPUT)
  {
    digitalWrite(PIN_VAP_PULSE, val);
  }
}

void init_FS(void)
{
  DBG_printf(dsDefault, "Init file system (FS) ... ");
  if (SPIFFS.begin())
  {
    DBG_printf(dsDefault, "OK \n");
  }
  else 
  {
    DBG_printf(dsDefault, "ERROR !\n");
  }  
}

void init_I2C(void)
{
  DBG_printf(dsDefault, "Init I2C (SDA=D1, SCL=D2) ... ");
  Wire.begin(D1, D2);
  DBG_printf(dsDefault, "OK \n");
}

void setVapPulse(uint8 state)
{
  digitalWrite(PIN_VAP_PULSE, state);
}

void toggleVapPulse()
{
  digitalWrite(PIN_VAP_PULSE, !digitalRead(PIN_VAP_PULSE));
}

void init_amp_IO(void)
{
  pinMode(PIN_AMP_CONTROL, OUTPUT);
  digitalWrite(PIN_AMP_CONTROL, 1); //turn OFF
}

void setAmpTurnOn(void)
{
  digitalWrite(PIN_AMP_CONTROL, 0);
}

void setAmpTurnOff(void)
{
  digitalWrite(PIN_AMP_CONTROL, 1);
}

void ISRevntSystem(void)
{
  float fThr;
  uint16 iThr;

  //increment ticker counters
  tck.ticCntSystem++; 
  tck.ticCntPulse++;  
  tck.ticCntStatus++;
  tck.ticCntTaskUpdate++;

  // thr = (1 / system_tick_T) / (refresh_f)
  //# sytem (date time), 1Hz
  fThr = ((1000 / TICKER_MS_TIME_25) / 1);
  iThr = round(fThr);  
  if (tck.ticCntSystem >= iThr)
  {
    tck.ticCntSystem = 0;
    tck.doSystem = true;
  }

  //# pulse (0,1,2,3,4,5 Hz)
  if (global.task_active_idx > 0)
  {
    if (global.task_active_pulse_thr > 0)
    {      
      fThr = global.task_active_pulse_thr;
      iThr = round(fThr);
      if (tck.ticCntPulse >= iThr)
      {      
        tck.ticCntPulse = 0;
        tck.doPulse = true;
      }
    }
  }

  //# update task (2Hz)
  if (global.task_cnt > 0)
  {
    fThr = ((1000 / TICKER_MS_TIME_25) / 2);
    iThr = round(fThr);
    if (tck.ticCntTaskUpdate >= iThr)
    {
      tck.ticCntTaskUpdate = 0;
      tck.doTaskUpdate = true;
    }
  }

  //# update status (1/60 Hz -> 60s)
  fThr = ((1000 / TICKER_MS_TIME_25) / ((float)1/60));
  iThr = round(fThr);
  if (tck.ticCntStatus >= iThr)
  {
    //DBG_printf(dsDetails, "Update status threshold = %d \n", iThr);
    tck.ticCntStatus = 0;
    tck.doStatusUpdate = true;
  }  
}

void start_system_ticker(void)
{
  DBG_printf(dsDefault, "Start system timer ... ");

  tck.ticCntSystem = 0;
  tck.ticCntPulse = 0;
  tck.ticCntStatus = 0;
  tck.ticCntTaskUpdate = 0;
  tck.doSystem = false;
  tck.doPulse = false;
  tck.doStatusUpdate = false;
  tck.doTaskUpdate = false;  
  tck.doRestartRunningTask = false;
  p_ticker = NULL;

  //create ticker  
  p_ticker = new Ticker();
  
  //Initialize Ticker
  (*p_ticker).attach_ms(TICKER_MS_TIME_25, ISRevntSystem);

  DBG_printf(dsDefault, "OK \n");
}

void clear_eeprom(void)
{
  wdtSoftDisable(dbgSetup);
  wdtReset(dbgSetup);  
  DBG_printf(dsDefault, "clear_eeprom() - reset settings ... ");

  uint16 i;
  for (i = 0; i < EEPROM_SIZE; i++)
  {
    EEPROM.write(i, 0xFF);
  }
  EEPROM.commit();

  DBG_printf(dsDefault, "OK \n");
  wdtReset(dbgSetup);
  wdtSoftEnable(dbgSetup);
}

void read_eeprom_settings(bool clrEEPROM)
{
  char mode[20] = "";
  char status[20] = "";

  wdtSoftDisable(dbgSetup);
  wdtReset(dbgSetup);
  DBG_printf(dsDefault, "Load settings from EEPROM ... \n");

  DBG_printf(dsDefault, "Init EEPROM (%d of the FLASH) ... \n", EEPROM_SIZE);
  EEPROM.begin(EEPROM_SIZE); //eeprom (emulation, 512B after SPIFF location)
  delay(10);

  if (clrEEPROM)
  {
    clear_eeprom();
  }

  settings.in_eeprom = EEPROM.read(0);
  if (settings.in_eeprom == 1)
  {    
    EEPROM.get(0, settings);

    DBG_printf(dsDetails, "in_eeprom = \"%d\" \n", settings.in_eeprom);
    wdtReset(dbgSetup);
    //
    switch (settings.mode)
    {
    case vmAP:
      strlcpy(&mode[0], "ap", sizeof(mode));
      break;
    case vmClient:
      strlcpy(&mode[0], "client", sizeof(mode));
      break;
    default:
      strlcpy(&mode[0], "ap", sizeof(mode));
    }
    DBG_printf(dsDetails, "mode = \"%s\" \n", mode);
    wdtReset(dbgSetup);
    //
    switch (settings.vap_stat)
    {
    case vsStarted:
      strlcpy(&status[0], "started", sizeof(status));
      break;
    case vsStopped:
      strlcpy(&status[0], "stopped", sizeof(status));
      break;
    default:
      strlcpy(&status[0], "stopped", sizeof(status));
    }
    DBG_printf(dsDetails, "status = \"%s\" \n", status);
    wdtReset(dbgSetup);
    //
    DBG_printf(dsDetails, "ssid = \"%s\" \n", settings.ssid);
    wdtReset(dbgSetup);
    DBG_printf(dsDetails, "pass = \"%s\" \n", settings.pass);
    wdtReset(dbgSetup);
    DBG_printf(dsDetails, "use_dhcp = \"%d\" \n", settings.use_dhcp);
    wdtReset(dbgSetup);
    DBG_printf(dsDetails, "cli_ip = \"%s\" \n", settings.cli_ip);
    wdtReset(dbgSetup);
    DBG_printf(dsDetails, "cli_subnet = \"%s\" \n", settings.cli_subnet);
    wdtReset(dbgSetup);
    DBG_printf(dsDetails, "cli_gateway = \"%s\" \n", settings.cli_gateway);
    wdtReset(dbgSetup);
    DBG_printf(dsDetails, "cli_dns = \"%s\" \n", settings.cli_dns);
    wdtReset(dbgSetup);
    //
    DBG_printf(dsDetails, "login = \"%s\" \n", settings.vapLogin);
    wdtReset(dbgSetup);
    DBG_printf(dsDetails, "password = \"%s\" \n", settings.vapPass);
    wdtReset(dbgSetup);
    //
    DBG_printf(dsDetails, "summer time = \"%d\" \n", settings.isSummerTime);
    wdtReset(dbgSetup);    
  }
  else
  {
    DBG_printf(dsDetails, "Settings not in the eeprom - load default values. \n");
    settings.mode = vmAP;
    strlcpy(&settings.ssid[0], "Vaporizer 0.1", sizeof(settings.ssid));
    strlcpy(&settings.pass[0], "", sizeof(settings.pass));
    settings.use_dhcp = 1;
    settings.vap_stat = vsStopped;
    strlcpy(&settings.vapLogin[0], "admin", sizeof(settings.vapLogin));
    strlcpy(&settings.vapPass[0], "", sizeof(settings.vapPass));
    settings.isSummerTime = false;
  }

  EEPROM.end();
  DBG_printf(dsDefault, "EEPROM settings loaded - OK \n");
  wdtSoftEnable(dbgSetup);
}

float read_dht22_temperature(void)
{
  DBG_printf(dsDefault, "Read DHT22 temperature ... ");
  float res = -1;
  
  wdtSoftDisable(dbgSetup);
  wdtReset(dbgSetup);

  //p_dht22 = new DHT();
  //(*p_dht22).setup(PIN_DHT22, DHT::DHT22);

  if (p_dht22 != NULL)
  {
    res = (*p_dht22).getTemperature();
  }

  //delete p_dht22;
  //p_dht22 = NULL;

  DBG_printf(dsDefault, "%.2f C \n", res);

  wdtSoftEnable(dbgSetup);
  return res;
}

float read_dht22_humidity(void)
{
  DBG_printf(dsDefault, "Read DHT22 humidity ... ");
  float res = -1;

  //p_dht22 = new DHT();
  //(*p_dht22).setup(PIN_DHT22, DHT::DHT22);

  if (p_dht22 != NULL)
  {
    res = (*p_dht22).getHumidity();
  }

  //delete p_dht22;
  //p_dht22 = NULL;

  DBG_printf(dsDefault, "%.2f %% \n", res);
  return res;
}

float read_battery_voltage(void)
{
  DBG_printf(dsDefault, "Read battery voltage ... ");  
  uint16 val = 0;
  float res = -1;

  val = analogRead(A0); // 3.2V = 1023 (0x03FF)
  
  //3.2V = 14.45V
  res = (((float)14.45 * val) / 1023);

  DBG_printf(dsDefault, "%.2f V \n", res);
  return res;
}

void read_status(void)
{
  global.temperature = read_dht22_temperature();
  global.humidity = read_dht22_humidity();
  global.battery_voltage = read_battery_voltage();  
}

void init_dht22(void)
{
  p_dht22 = new DHT();
  (*p_dht22).setup(PIN_DHT22, DHT::DHT22);  
}

void init_ad9833(void)
{
  DBG_printf(dsDefault, "Init AD9833 ... ");

  #define DATA  13	///< SPI Data pin number (MOSI)
  #define CLK   14	///< SPI Clock pin number (CLK)
  #define FSYNC 15	///< SPI Load pin number (CS)

  p_ad9833 = new MD_AD9833(FSYNC); //hardware SPI
  //p_ad9833 = new MD_AD9833(DATA, CLK, FSYNC); //software SPI

  (*p_ad9833).begin();  //SPI init
  (*p_ad9833).setMode(MD_AD9833::MODE_OFF); //stop generator

  DBG_printf(dsDefault, "OK \n");
}

void set_ad9833_freq(float aFreq, bool doDebug)
{
  if (!doDebug)
  {
    wdtSoftDisable(dbgSetup);
    wdtReset(dbgSetup);
  }

  if (doDebug)
  {
    DBG_printf(dsDefault, "Set AD9833 freq=%.2f Hz ... ", aFreq);
  }

  if (p_ad9833 != NULL)
  {
    if (aFreq > 0)
    {
      //sine mode
      if ((*p_ad9833).getMode() != MD_AD9833::MODE_SINE)
      {
        (*p_ad9833).setMode(MD_AD9833::MODE_SINE);
      }
      
      //set freq
      if ((*p_ad9833).setFrequency(MD_AD9833::CHAN_0, aFreq))
      {
        if (doDebug)
        {
          DBG_printf(dsDefault, "OK \n");
        }
      }
      else
      {
        if (doDebug)
        {
          DBG_printf(dsDefault, "ERROR \n");
        }
      }      
    }
    else
    {
      //0 Hz -> stop generator
      (*p_ad9833).setMode(MD_AD9833::MODE_OFF);
      if (doDebug)
      {
        DBG_printf(dsDefault, "OK \n");
      }
    }
  }
  else
  {
    if (doDebug)
    {
      DBG_printf(dsDefault, "ERROR \n");
    }
  }

  if (!doDebug)
  {
    wdtSoftEnable(dbgSetup);
  }
}

void read_ds3231_datetime(void)
{
  DBG_printf(dsDefault, "Read date time from DS3231 ... ");
  p_rtc = new RTC_DS3231();  
  //
  DateTime dt;
  dt = (*p_rtc).now();
  //
  global.dYear = dt.year();
  global.dMonth = dt.month();
  global.dDay = dt.day();
  global.tHour = dt.hour();
  global.tMin = dt.minute();
  global.tSec = dt.second();
  //
  delete p_rtc;
  p_rtc = NULL;
  DBG_printf(dsDefault, "%02d.%02d.%04d %02d:%02d:%02d, summer=%d \n",
    global.dDay, global.dMonth, global.dYear, global.tHour, global.tMin, global.tSec, settings.isSummerTime
  );
}

void write_ds3231_datetime_settings(void)
{
  wdtSoftDisable(dbgSetup);

  DBG_printf(dsDefault, "Write date time to DS3231 ... %02d.%02d.%04d %02d:%02d:%02d, summer=%d ... ",
    global.dDay, global.dMonth, global.dYear, global.tHour, global.tMin, global.tSec, settings.isSummerTime
  );
  p_rtc = new RTC_DS3231();  
  //
  wdtReset(dbgSetup);
  (*p_rtc).adjust(DateTime( global.dYear, global.dMonth, global.dDay, global.tHour, global.tMin, global.tSec ));
  //
  delete p_rtc;
  p_rtc = NULL;
  DBG_printf(dsDefault, "OK \n");

  wdtSoftEnable(dbgSetup);
}

void write_eeprom_settings(void)
{
  char mode[20] = "";
  char status[20] = "";

  wdtSoftDisable(dbgSetup);
  wdtReset(dbgSetup);
  DBG_printf(dsDefault, "Write settings to EEPROM ... \n");

  DBG_printf(dsDefault, "Init EEPROM (%d of the FLASH) ... \n", EEPROM_SIZE);
  EEPROM.begin(EEPROM_SIZE); //eeprom (emulation, 512B after SPIFF location)
  delay(10);
  
  settings.in_eeprom = 1;
  
  DBG_printf(dsDetails, "in_eeprom = \"%d\" \n", settings.in_eeprom);
  wdtReset(dbgSetup);
  //
  switch (settings.mode)
  {
  case vmAP:
    strlcpy(&mode[0], "ap", sizeof(mode));
    break;
  case vmClient:
    strlcpy(&mode[0], "client", sizeof(mode));
    break;
  default:
    strlcpy(&mode[0], "ap", sizeof(mode));
  }  
  DBG_printf(dsDetails, "mode = \"%s\" \n", mode);  
  wdtReset(dbgSetup);
  //
  switch (settings.vap_stat)
  {
  case vsStarted:
    strlcpy(&status[0], "started", sizeof(status));
    break;
  case vsStopped:
    strlcpy(&status[0], "stopped", sizeof(status));
    break;
  default:
    strlcpy(&status[0], "stopped", sizeof(status));
  }
  DBG_printf(dsDetails, "status = \"%s\" \n", status);
  wdtReset(dbgSetup);
  //
  DBG_printf(dsDetails, "ssid = \"%s\" \n", settings.ssid);
  wdtReset(dbgSetup);
  DBG_printf(dsDetails, "pass = \"%s\" \n", settings.pass);
  wdtReset(dbgSetup);
  DBG_printf(dsDetails, "use_dhcp = \"%d\" \n", settings.use_dhcp);
  wdtReset(dbgSetup);
  DBG_printf(dsDetails, "cli_ip = \"%s\" \n", settings.cli_ip);
  wdtReset(dbgSetup);
  DBG_printf(dsDetails, "cli_subnet = \"%s\" \n", settings.cli_subnet);
  wdtReset(dbgSetup);
  DBG_printf(dsDetails, "cli_gateway = \"%s\" \n", settings.cli_gateway);
  wdtReset(dbgSetup);
  DBG_printf(dsDetails, "cli_dns = \"%s\" \n", settings.cli_dns);
  wdtReset(dbgSetup);
  //
  DBG_printf(dsDetails, "login = \"%s\" \n", settings.vapLogin);
  wdtReset(dbgSetup);
  DBG_printf(dsDetails, "password = \"%s\" \n", settings.vapPass);
  wdtReset(dbgSetup);
  //
  DBG_printf(dsDetails, "summer time = \"%d\" \n", settings.isSummerTime);
  wdtReset(dbgSetup);

  //write
  //EEPROM.write(addr, val);  
  EEPROM.put(0, settings);
  if (EEPROM.commit())
  {
    DBG_printf(dsDefault, "Settings save - OK \n");
  }
  else
  {
    DBG_printf(dsDefault, "Settings save - ERROR! \n");
  }
  EEPROM.end();

  wdtSoftEnable(dbgSetup);
}

bool IsTaskScheduled(t_task *p_aTsk)
{
  bool res = false;
  if (p_aTsk != NULL)
  {
    res = ((*p_aTsk).schedule & ((uint32)1 << global.tHour));
    DBG_printf(dsDetails, "Task schedule=\"%u\", actual time schedule=\"%u\", result=%d \n", 
      (*p_aTsk).schedule,
      ((uint32)1 << global.tHour),
      res
    );
  }
  return res;
}

t_task* GetActiveTask(void)
{
  t_task *p_tskRes = NULL;

  if (global.task_cnt > 0)
  {
    if (global.task_active_idx > 0)
    {
      p_tskRes = &p_task_list[global.task_active_idx - 1];
    }
  }  

  return p_tskRes;
}

t_task* GetNextTask(t_task *p_aTsk)
{
  DBG_printf(dsDetails, "GetNextTask, active_task=%d \n", (p_aTsk != NULL));

  uint8 i;  
  uint8 idx;
  uint8 cnt;
  t_task *p_tsk = NULL;
  t_task *p_tskRes = NULL;

  if (global.task_cnt > 0)
  {
    if (p_aTsk != NULL)
    {
      p_tsk = p_aTsk;
    }

    if (p_tsk == NULL)
    {
      //first enabled or running task
      for (i = 1; i <= global.task_cnt; i++)
      {
        p_tskRes = &p_task_list[i - 1];
        //task is enabled
        if ((*p_tskRes).status == tsEnabled)
        {
          //task is schedulled
          if (
            IsTaskScheduled(p_tskRes) //task is schedulled
            && (!TaskIsPaused(p_tskRes)) //task is not paused
          )
          {
            break; //task founded, end FOR
          }
          else
          {
            p_tskRes = NULL;
          }
        }
        else
        {
          p_tskRes = NULL;
        }        
      } //end - for
    }
    else
    {
      //next enabled task
      cnt = 1;
      idx = (*p_tsk).idx;
      while (cnt <= global.task_cnt)
      {
        i = idx + cnt; //next task

        //trim index
        if (i > global.task_cnt)
        {
          i = i % global.task_cnt;
          if (i == 0) {
            i = global.task_cnt;
          }
        }

        p_tskRes = &p_task_list[i - 1]; //select task

        //task is enabled
        if ((*p_tskRes).status == tsEnabled)
        {          
          if (
            IsTaskScheduled(p_tskRes) //task is schedulled
            && (!TaskIsPaused(p_tskRes)) //task is not paused
          )
          {
            break; //next enabled/running and scheduled task founded -> end FOR
          }
          else
          {
            p_tskRes = NULL;
          }
        }
        else 
        {
          p_tskRes = NULL;
        }

        cnt++;
      } //end - while
    } //end - next task
  } //end - task cnt > 0

  return p_tskRes;
}

void SetActiveTask(t_task *p_aTsk)
{
  ClearActiveTask();
  if (p_aTsk != NULL)
  {       
    global.task_active_idx = (*p_aTsk).idx;
    global.task_active_freq = (*p_aTsk).freqStart;
    if ((*p_aTsk).freqStart < (*p_aTsk).freqStop)
    {
      global.task_active_freq_step = ( ((*p_aTsk).freqStop - (*p_aTsk).freqStart) / (*p_aTsk).duration ) / 2; //update freq is 2 Hz -> divide by 2
      DBG_printf(dsDefault, "SetActiveTask() - \"%s\", freq <%.2f, %.2f> [%.2f], duration=%u s, pause=%u ... \n",
        (*p_aTsk).name,
        (*p_aTsk).freqStart,
        (*p_aTsk).freqStop,
        global.task_active_freq_step,
        (*p_aTsk).duration,
        (*p_aTsk).pause
      ); 
    }
    else
    {
      global.task_active_freq_step = 0;      
    }

    if ((*p_aTsk).freqPulse > 0)
    {
      global.task_active_pulse_thr = ((1000 / TICKER_MS_TIME_25) / ((*p_aTsk).freqPulse * 2));
    }
    else
    {      
      global.task_active_pulse_thr = 0;
      tck.doPulse = false;
      setVapPulse(0); //enable output
    }

    setAmpTurnOn();
    set_ad9833_freq(global.task_active_freq, false);
  }  
}

bool IsActiveTaskInProgress(void)
{
  bool res = false;

  if (global.task_cnt > 0)
  {
    if (global.task_active_idx > 0)
    {
      res = (p_task_list[global.task_active_idx - 1].status == tsEnabled) 
        && (global.task_active_duration <= p_task_list[global.task_active_idx - 1].duration);
    }
  }

  return res;
}

void read_task_list(void)
{
  uint8 tskCnt;
  uint8 sched;
  uint16 j;
  uint32 ram;
  uint32 task_ram;

  File f;
  char row[512];
  char tmp[256];
  size_t rowLen;

  DBG_printf(dsDefault, "read_task_list() - loading task list from FS ... \n");

  //pause vaporizer
  if (settings.vap_stat == vsStarted)
  {    
    settings.vap_stat = vsPaused;
  }

  //clear task list  
  ClearActiveTask();
  if (p_task_list != NULL)
  { 
    tskCnt = global.task_cnt;
    global.task_cnt = 0; //set task count to 0 before destroy task list .. i dont want to raise an exception
    DBG_printf(dsDefault, "Clear task list (%d -> %d) ... \n", tskCnt, 0);
    p_task_list = taskListSetSize(p_task_list, global.task_cnt, 0);
    DBG_printf(dsDefault, "Task list is empty ... \n");
  }
  else
  {
    global.task_cnt = 0;  
  }

  //load task list
  ram = ESP.getFreeHeap();
  task_ram = 0;
  tskCnt = 0;

  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "Open task direcotry \"%s\" \n", TASK_DEF_DIR);
  Dir def_dir = SPIFFS.openDir(TASK_DEF_DIR);
  while (def_dir.next())
  {
    wdtReset(dbgSetup);  

    str_init(&row[0], sizeof(row));
    strlcat(&row[0], def_dir.fileName().c_str(), sizeof(row));
    str_init(&tmp[0], sizeof(tmp));
    strlcat(&tmp[0], TASK_DEF_DIR, sizeof(tmp));    
    str_replace(&row[0], sizeof(row), tmp, "");
    DBG_printf(dsDefault, "Task definition \"%s\" found \n", row);

    rowLen = strlen(row); //get file name length (length is without "\0")
    if (rowLen > 0)
    {
      //allocate memory for task
      p_task_list = taskListSetSize(p_task_list, tskCnt, tskCnt + 1);

      //fill task params
      if (p_task_list != NULL)
      {
        //idx
        p_task_list[tskCnt].idx = tskCnt + 1; //for simplest task list operations

        //uid
        DBG_printf(dsDetails, "Set task uid \"%s\" size \"%d\" ... \n", row, rowLen + 1);
        p_task_list[tskCnt].uid = str_setLength(p_task_list[tskCnt].uid, rowLen + 1); //alocate string memory, +1 for '\0'
        strlcpy(p_task_list[tskCnt].uid, row, rowLen + 1);                                     //copy name, +1 for '\0'

        str_init(&tmp[0], sizeof(tmp));
        strlcat(&tmp[0], TASK_DEF_DIR, sizeof(tmp));
        strlcat(&tmp[0], p_task_list[tskCnt].uid, sizeof(tmp));
        //
        DBG_printf(dsDetails, "Open task definiton \"%s\" ... \n", p_task_list[tskCnt].uid);
        f = SPIFFS.open(tmp, "r");
        if (f)
        {
          DBG_printf(dsDetails, "Read task definiton \"%s\" parameters ... \n", p_task_list[tskCnt].uid);

          //status
          rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
          if (row[0] == '0')
          {
            p_task_list[tskCnt].status = tsDisabled;
          }
          else if (row[0] == '1')
          {
            p_task_list[tskCnt].status = tsEnabled;
          }
          else
          {
            p_task_list[tskCnt].status = tsUnknown;
          }

          //name
          rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
          p_task_list[tskCnt].name = str_setLength(p_task_list[tskCnt].name, rowLen + 1);
          strlcpy(p_task_list[tskCnt].name, &row[0], rowLen + 1);

          //description
          rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
          p_task_list[tskCnt].description = str_setLength(p_task_list[tskCnt].description, rowLen + 1);
          strlcpy(p_task_list[tskCnt].description, &row[0], rowLen + 1);

          //img
          rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
          p_task_list[tskCnt].img = str_setLength(p_task_list[tskCnt].img, rowLen + 1);
          strlcpy(p_task_list[tskCnt].img, &row[0], rowLen + 1);

          //duration
          rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
          if (!str2uint16(&row[0], &p_task_list[tskCnt].duration))
          {
            p_task_list[tskCnt].duration = 10;
          }

          //pause
          rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
          if (!str2uint16(&row[0], &p_task_list[tskCnt].pause))
          {
            p_task_list[tskCnt].pause = 0;
          }

          //start freq
          rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
          if (!str2float(&row[0], &p_task_list[tskCnt].freqStart))
          {
            p_task_list[tskCnt].freqStart = 400;
          }

          //stop freq
          rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
          if (!str2float(&row[0], &p_task_list[tskCnt].freqStop))
          {
            p_task_list[tskCnt].freqStop = 650;
          }

          //pulse freq
          rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
          if (!str2uint8(&row[0], &p_task_list[tskCnt].freqPulse))
          {
            p_task_list[tskCnt].freqPulse = 3;
          }

          //schedule
          p_task_list[tskCnt].schedule = 0;
          for (j = 0; j < 24; j++)
          {
            rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
            if (rowLen > 0)
            {
              if (str2uint8(&row[0], &sched))
              {
                p_task_list[tskCnt].schedule = p_task_list[tskCnt].schedule | ((uint32)sched << j);
              }
            }
            else
            {
              break;
            }
          }

          //close task definition
          f.close();
        }
        else
        {
          DBG_printf(dsDefault, "Error: can not open task file \"%s\"!\n", p_task_list[tskCnt].uid);
          break; // BREAK while
        }

        task_ram = ram - ESP.getFreeHeap();
        ram = ESP.getFreeHeap();
        DBG_printf(dsDefault, "Read task \"%s\" [used %d B of the RAM, remain %d B of the RAM] ... OK \n", p_task_list[tskCnt].uid, task_ram, ram);

        tskCnt++;
      }
      else
      {
        DBG_printf(dsDefault, "Can not allocate task memory. Not enought RAM (%d B)!", ESP.getFreeHeap());
        break; // BREAK while
      }
    }
    else
    {
      DBG_printf(dsDefault, "Empty task name \"%s\"!\n", "");
    }

    if (tskCnt >= TASK_MAX_CNT)
    {
      break;  //end while
    }
  } //end-while

  global.task_cnt = tskCnt;
  DBG_printf(dsDefault, "%d tasks founded ... \n", global.task_cnt);

  //pause vaporizer
  if (settings.vap_stat == vsPaused)
  {
    settings.vap_stat = vsStarted;
  }

  wdtSoftEnable(dbgSetup);
}

bool saveTask(t_task *tsk)
{
  DBG_printf(dsDefault, "saveTask() - \"%s\" \n", (*tsk).uid);

  char tmp[255] = "";
  File f;
  uint8 i;

  if (
    ((*tsk).uid == NULL)
    || ((*tsk).name == NULL)
    || ((*tsk).img == NULL)
    || ((*tsk).description == NULL)
  )
  {
    DBG_printf(dsDefault, "Some of the important parameters of the task are missing!");
    return false;
  }

  str_init(&tmp[0], sizeof(tmp));
  strlcat(&tmp[0], TASK_DEF_DIR, sizeof(tmp));
  strlcat(&tmp[0], (*tsk).uid, sizeof(tmp));

  f = SPIFFS.open(tmp, "w");
  if (f)
  {
    //save data
    if ((*tsk).status == tsDisabled) {
      f.println("0");
    }
    else
    {
      f.println("1");
    }
    f.println((*tsk).name);
    f.println((*tsk).description);
    f.println((*tsk).img);
    f.printf("%d\r\n", (*tsk).duration);
    f.printf("%d\r\n", (*tsk).pause);    
    f.printf("%.2f\r\n", (*tsk).freqStart);
    f.printf("%.2f\r\n", (*tsk).freqStop);
    f.printf("%d\r\n", (*tsk).freqPulse);
    //
    for (i = 0; i < 24; i++)
    {
      f.printf("%d\r\n", ((*tsk).schedule >> i) & 0x00000001);
      wdtReset(dbgSetup);
    }

    f.close();
    DBG_printf(dsDefault, "File \"%s\" saved. \n", (*tsk).uid);
    return true;
  }
  else
  {
    DBG_printf(dsDefault, "File \"%s\" not found! \n", (*tsk).uid);
    return false;
  }
}

bool handle_FileRead(String path)
{
  DBG_printf(dsDetails, "Do \"handleFileRead()\" \n");
  if (path.endsWith("/"))
  {
    path = path + "index.html";
  }

  String contentType = getContentType(path);
  if (contentType.length() > 0)
  {
    if (SPIFFS.exists(path))
    {
      File file = SPIFFS.open(path, "r");
      if (file)
      {
        DBG_printf(dsDetails, "Stream file \"%s\". \n", path.c_str());
        (*p_srv).streamFile(file, contentType);
        file.close();
        return true;
      }
      else
      {
        DBG_printf(dsDefault, "Error while opennig file \"%s\"! \n", path.c_str());
      }
    }
    else
    {
      DBG_printf(dsDefault, "File \"%s\" not found! \n", path.c_str());
    }
  }
  else
  {
    DBG_printf(dsDefault, "Forbidden file \"%s\" not found! \n", path.c_str());
  }

  return false;
}

void srvHandle_NotFound_Static()
{
  (*p_srv).send(404, "text/plain", "404: Not Found");
}

bool IsServerAutenticated(void)
{
  bool res = false;

  //check authentication
  if (
    (strlen(&settings.vapLogin[0]) > 0)
    && (strlen(&settings.vapPass[0]) > 0)
  )
  {
    DBG_printf(dsDefault, "IsServerAutenticated() - \"%s\"/\"%s\" \n", settings.vapLogin, settings.vapPass);
    res = (*p_srv).authenticate(&settings.vapLogin[0], &settings.vapPass[0]);  
  }
  else
  {
    res = true; //no login or login+password set -> no authentication
  }
  
  return res;
}

void DoServerAutentication(void)
{
  DBG_printf(dsDefault, "DoServerAutentication() - call autentication \n");
  (*p_srv).requestAuthentication();
}

void srvHandle_NotFound()
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  if (!handle_FileRead((*p_srv).uri()))
  {
    srvHandle_NotFound_Static();
  }
}

void srvHandle_Root()
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  DBG_printf(dsDefault, "Do \"srvHandleRoot()\" \n");
  File f_root = SPIFFS.open(HTML_ROOT, "r");
  if (f_root)
  {
    (*p_srv).streamFile(f_root, "text/html");
    DBG_printf(dsDefault, "Return file \"%s\". \n", HTML_ROOT);
  }
  else
  {
    DBG_printf(dsDefault, "File \"%s\" not found! \n", HTML_ROOT);
    (*p_srv).send(404, "text/plain", "404: Not Found");
  }
}

void srvHandle_RedirectTo(char *uri)
{
  DBG_printf(dsDefault, "srvHandle_RedirectTo() - \"%s\" \n", uri);

  (*p_srv).sendHeader("Location", uri, true);
  (*p_srv).send(302, "text/plane","");   
}

void srvHandle_Start(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  DBG_printf(dsDefault, "srvHandle_Start() \n");
  settings.vap_stat = vsStarted;  
  write_eeprom_settings();
  
  tck.doTaskUpdate = true; //UpdateTask will be triggered
  srvHandle_RedirectTo(strdup(HTML_ROOT));  //strdup - returns pointer to null-terminated byte string  
}

void srvHandle_Stop(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  DBG_printf(dsDefault, "srvHandle_Stop() \n");
  settings.vap_stat = vsStopped;    
  write_eeprom_settings();
  
  srvHandle_RedirectTo(strdup(HTML_ROOT));
}

void srvHandle_Buttons(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  uint8 btnStart;
  uint8 btnStop;
  char str[100] = "";

  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "srvHandle_Buttons() \n");

  //data
  if (settings.vap_stat == vsStarted)
  {
    btnStart = 0;
    btnStop = 1;
  }
  else if (settings.vap_stat == vsStopped)
  {
    btnStart = 1;
    btnStop = 0;
  }
  else
  {
    btnStart = 0;
    btnStop = 0;
  }
  snprintf(&str[0], sizeof(str), "[{\"btn_start\":\"%d\", \"btn_stop\":\"%d\"}]", btnStart, btnStop);

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(strlen(&str[0]));
  //data
  DBG_printf(dsUltimateDetails, str);
  DBG_printf(dsUltimateDetails, "\n");
  (*p_srv).send(200, "application/json", str);  

  wdtSoftEnable(dbgSetup);
}

void srvHandle_Voltage(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[100] = "";

  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "srvHandle_Voltage() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "application/json", "");

  //data
  snprintf(&str[0], sizeof(str), "[{\"voltage\":\"%.1f\"}]", global.battery_voltage);
  (*p_srv).sendContent(str);

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();

  wdtSoftEnable(dbgSetup);
}

void srvHandle_Humidity(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[100] = "";

  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "srvHandle_Humidity() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "application/json", "");

  //data
  snprintf(&str[0], sizeof(str), "[{\"humidity\":\"%.0f\"}]", global.humidity);
  (*p_srv).sendContent(str);

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();

  wdtSoftEnable(dbgSetup);
}

void srvHandle_Temperature(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[100] = "";

  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "srvHandle_Temperature() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "application/json", "");

  //data
  snprintf(&str[0], sizeof(str), "[{\"temperature\":\"%.1f\"}]", global.temperature);
  (*p_srv).sendContent(str);

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();

  wdtSoftEnable(dbgSetup);
}

void srvHandle_ActualTask(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[1024] = "";

  DBG_printf(dsDefault, "srvHandle_ActualTask() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "application/json", "");

  //data
  if (
      (settings.vap_stat == vsStarted)
      && (global.task_cnt > 0)
      && (global.task_active_idx > 0)
    )
  {
    snprintf(&str[0], sizeof(str), "[{\"name\":\"%s\", \"description\":\"%s\", \"frequency\":\"%.2f\"}]",
             p_task_list[global.task_active_idx - 1].name,
             p_task_list[global.task_active_idx - 1].description,
             global.task_active_freq);
  }
  else
  {
    snprintf(&str[0], sizeof(str), "[{\"name\":\"%s\", \"description\":\"%s\", \"frequency\":\"%s\"}]", "?", "?", "?");
  }
  (*p_srv).sendContent(str);

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();
}

void srvHandle_TaskList(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[512] = "";
  char statImg[256] = "";
  uint8 i;
  t_task *tsk;

  DBG_printf(dsDefault, "srvHandle_TaskList() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "application/json", "");

  //data
  if (global.task_cnt > 0)
  {
    (*p_srv).sendContent("[");    
    // ---
    for (i = 0; i < global.task_cnt; i++)
    {
      tsk = &p_task_list[i];

      if ((*tsk).idx == global.task_active_idx)
      {
        strlcpy(&statImg[0], IMG_STAT_IN_PROGRESS, sizeof(statImg));
      }
      else if ((*tsk).status == tsEnabled)
      {
        strlcpy(&statImg[0], IMG_STAT_ENABLED, sizeof(statImg));
      }
      else
      {
        strlcpy(&statImg[0], IMG_STAT_DISABLED, sizeof(statImg));
      }

      snprintf(&str[0], sizeof(str), "{\"uid\":\"%s\", \"task_status_img\":\"%s\", \"task_img\":\"%s\", ", (*tsk).uid, statImg, (*tsk).img);
      (*p_srv).sendContent(str);

      snprintf(&str[0], sizeof(str), "\"task_name\":\"%s\", ", (*tsk).name);
      (*p_srv).sendContent(str);

      snprintf(&str[0], sizeof(str), "\"task_desc\":\"%s\", ", (*tsk).description);
      (*p_srv).sendContent(str);
      
      snprintf(&str[0], sizeof(str), "\"freq_start\":\"%.2f\", \"freq_stop\":\"%.2f\", \"task_pulse\":\"%d\", \"task_duration\":\"%u\", \"task_pause\":\"%u\" }",
        (*tsk).freqStart, (*tsk).freqStop, (*tsk).freqPulse, (*tsk).duration, (*tsk).pause
      );
      (*p_srv).sendContent(str);      

      if ((i + 1) < global.task_cnt)
      {
        (*p_srv).sendContent(", ");
      }
    }
    // ---
    (*p_srv).sendContent("]");
  }
  
  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();
}

void srvHandle_TaskImgList(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[512] = "";
  char img[256] = "";
  char sep[3] = "";

  DBG_printf(dsDefault, "srvHandle_TaskImgList() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "application/json", "");

  //data
  Dir d = SPIFFS.openDir(IMG_DIR_TASK);
  (*p_srv).sendContent("[");
  while (d.next())
  {
    if (strlen(sep) > 0)  
    {
      (*p_srv).sendContent(sep);
    }
    else
    {
      str_init(&sep[0], sizeof(sep));
      strlcat(&sep[0], ", ", sizeof(sep));
    }
    str_init(&str[0], sizeof(str));
    strlcat(&str[0], d.fileName().c_str(), sizeof(str));
    str_replace(&str[0], sizeof(str), IMG_DIR_TASK, "");

    str_init(&img[0], sizeof(img));
    strlcat(&img[0], str, sizeof(img));
    DBG_printf(dsUltimateDetails, "img_name = %s \n", img);

    str_init(&str[0], sizeof(str));
    snprintf(&str[0], sizeof(str), "{\"val\":\"%s\"}", img);
    (*p_srv).sendContent(str);    
  }
  (*p_srv).sendContent("]");
  
  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();
}

void srvHandle_TaskEdit(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char task_uid[50] = "";
  char tmp[256] = "";
  char row[1024] = "";
  size_t rowLen = 0;
  bool bTaskUid_Found = false;
  File f;

  DBG_printf(dsDefault, "srvHandle_TaskEdit() ... ");
  if ((*p_srv).hasArg("task_uid"))
  {
    strlcpy(&task_uid[0], (*p_srv).arg("task_uid").c_str(), sizeof(task_uid));    
    DBG_printf(dsDefault, "task_uid = %s ... ", task_uid);

    //header
    (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    (*p_srv).sendHeader("Pragma", "no-cache");
    (*p_srv).sendHeader("Expires", "-1");
    (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
    (*p_srv).send(200, "text/html", "");

    //data
    str_init(&tmp[0], sizeof(tmp));
    strlcat(&tmp[0], HTML_SETTINGS_TASK, sizeof(tmp)); //concatenate SRC (param 2) to DSC (param 1)
    f = SPIFFS.open(tmp, "r");
    if (f)
    {
      rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
      while (rowLen > 0)
      { 
        if (!bTaskUid_Found)
        {
          //only 1 replacement in whole file .. dont waste the time for searching next occurence
          bTaskUid_Found = str_replace(&row[0], sizeof(row), "[TASK_UID]", task_uid);  
        }        
        (*p_srv).sendContent(row);
        //
        rowLen = FS_ReadLine_CRLF(&f, &row[0], sizeof(row), dbgSetup);
      }

      f.close();
      DBG_printf(dsDefault, "OK \n");
    }
    else
    {
      DBG_printf(dsDefault, "ERROR \n");
    }

    //finalize response
    (*p_srv).sendContent("");
    (*p_srv).client().stop();
  }
  else
  {
    DBG_printf(dsDefault, "task_uid NOT DEFINED! \n");
    srvHandle_NotFound_Static();
  }  
}

void srvHandle_TaskGet(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[512] = "";
  char taskUid[50] = "";
  t_task *tsk = NULL;
  uint8 i;

  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "srvHandle_TaskGet() \n");

  //read params
  //get task UID (task file name)
  if ((*p_srv).hasArg("task_uid"))
  {
    (*p_srv).arg("task_uid").toCharArray(&taskUid[0], sizeof(taskUid));
  }

  //find task
  for (i = 0; i < global.task_cnt; i++)
  {
    if (strcmp(p_task_list[i].uid, taskUid) == 0)
    {
      tsk = &p_task_list[i];
      break;
    }
    wdtReset(dbgSetup);
  }

  if (tsk != NULL)
  {
    //header
    (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    (*p_srv).sendHeader("Pragma", "no-cache");
    (*p_srv).sendHeader("Expires", "-1");
    (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
    (*p_srv).send(200, "application/json", "");

    //data
    (*p_srv).sendContent("["); //array start
    //
    snprintf(&str[0], sizeof(str), "{\"task_active\":\"%d\", \"task_name\":\"%s\", ", ((*tsk).status == tsEnabled), (*tsk).name);
    (*p_srv).sendContent(str);
    snprintf(&str[0], sizeof(str), "\"task_desc\":\"%s\", ", (*tsk).description);
    (*p_srv).sendContent(str);
    snprintf(&str[0], sizeof(str), "\"task_img\":\"%s\", \"freq_start\":\"%.2f\", \"freq_stop\":\"%.2f\", \"task_pulse\":\"%d\", ", (*tsk).img, (*tsk).freqStart, (*tsk).freqStop, (*tsk).freqPulse);
    (*p_srv).sendContent(str);
    snprintf(&str[0], sizeof(str), "\"task_duration\":\"%d\", \"task_pause\":\"%d\", ", (*tsk).duration, (*tsk).pause);
    (*p_srv).sendContent(str);

    for (i = 0; i < 24; i++)
    {
      if (i < 23)
      {
        snprintf(&str[0], sizeof(str), "\"sched_%d\":\"%d\",  ", i, ((*tsk).schedule >> i) & 0x00000001);
      }
      else
      {
        snprintf(&str[0], sizeof(str), "\"sched_%d\":\"%d\" }", i, ((*tsk).schedule >> i) & 0x00000001);
      }
      (*p_srv).sendContent(str);
      wdtReset(dbgSetup);
    }
    //
    (*p_srv).sendContent("]"); //array stop

    //finalize response
    (*p_srv).sendContent("");
    (*p_srv).client().stop();
  }
  else
  {
    //task not found
    srvHandle_NotFound_Static();
  }

  wdtSoftEnable(dbgSetup);
}

void srvHandle_WifiGet(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[512] = "";
  char mode[8] = "";

  DBG_printf(dsDefault, "srvHandle_WifiGet() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "application/json", "");

  //data
  switch (settings.mode)
  {
  case vmAP:
    strlcpy(&mode[0], "ap", sizeof(mode));
    break;
  case vmClient:
    strlcpy(&mode[0], "client", sizeof(mode));
    break;
  default:
    strlcpy(&mode[0], "ap", sizeof(mode));
  }

  snprintf(&str[0], sizeof(str), "[{\"wifi_mode\":\"%s\", \"ssid\":\"%s\", \"password\":\"%s\", \"use_dhcp\":\"%d\", ",
           mode, settings.ssid, settings.pass, settings.use_dhcp);
  (*p_srv).sendContent(str);
  snprintf(&str[0], sizeof(str), "\"ip_address\":\"%s\", \"ip_gateway\":\"%s\", \"net_mask\":\"%s\", \"ip_dns\": \"%s\"}]",
           settings.cli_ip, settings.cli_gateway, settings.cli_subnet, settings.cli_dns);
  (*p_srv).sendContent(str);

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();
}

void srvHandle_SystemGet(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[512] = "";
  char sDate[11] = "";
  char sTime[9] = "";

  DBG_printf(dsDefault, "srvHandle_SystemGet() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "application/json", "");

  //data
  snprintf(&sDate[0], sizeof(sDate), "%02d.%02d.%04d", global.dDay, global.dMonth, global.dYear);
  snprintf(&sTime[0], sizeof(sTime), "%02d:%02d:%02d", global.tHour, global.tMin, global.tSec);
  snprintf(&str[0], sizeof(str), "[{\"sys_date\":\"%s\", \"sys_time\":\"%s\"}]", sDate, sTime);
  (*p_srv).sendContent(str);

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();
}

void srvHandle_SecurityGet(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char str[512] = "";

  DBG_printf(dsDefault, "srvHandle_SecurityGet() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "application/json", "");

  //data
  snprintf(&str[0], sizeof(str), "[{\"usr_name\":\"%s\"}]", settings.vapLogin);
  (*p_srv).sendContent(str);

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();
}

bool generateTaskUID(t_task *aTsk)
{
  bool res = false;
  char fileName[8] = "";  //MS-DOS file name, 8.3
  char tmp[50] = "";
  char sIdx[4] = "";
  uint8 sLen;
  uint8 idx;
  uint8 cnt;

  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "generateTaskUID() \n");

  //get task uid form task name
  strlcpy(&fileName[0], (*aTsk).name, sizeof(fileName));
  if (strlen(fileName) > 0)
  {
    str_sanitizeFileName(&fileName[0]);
  }  
  else
  {
    strlcpy(&fileName[0], "task", sizeof(fileName));
  }
  DBG_printf(dsDefault, "Get task name \"%s\" ... \n", fileName);
  
  //check if file exists
  cnt = 0;
  idx = 0;
  str_init(&sIdx[0], sizeof(sIdx));
  str_init(&tmp[0], sizeof(tmp));  
  strlcat(&tmp[0], TASK_DEF_DIR, sizeof(tmp));
  strlcat(&tmp[0], &fileName[0], sizeof(tmp));
  strlcat(&tmp[0], ".def", sizeof(tmp));
  DBG_printf(dsDefault, "Check if task definiton \"%s\" exists ... \n", tmp);
  res = !SPIFFS.exists(&tmp[0]);
  while (!res)
  {

    //file exists -> modify name
    idx++;
    str_init(&sIdx[0], sizeof(sIdx));
    snprintf(&sIdx[0], sizeof(sIdx), "%d", idx);
    DBG_printf(dsDefault, "New file name \"%s%s\" ... \n", fileName, sIdx);
    while (strlen(fileName) + strlen(sIdx) > 8)
    {      
      fileName[strlen(fileName)- 1] = '\0';
      DBG_printf(dsDefault, "Modify file name to \"%s%s\" ... \n", fileName, sIdx);
    }
    wdtReset(dbgSetup);

    //check if file exists
    str_init(&tmp[0], sizeof(tmp));
    strlcat(&tmp[0], TASK_DEF_DIR, sizeof(tmp));
    strlcat(&tmp[0], &fileName[0], sizeof(tmp));
    strlcat(&tmp[0], &sIdx[0], sizeof(tmp));
    strlcat(&tmp[0], ".def", sizeof(tmp));
    DBG_printf(dsDefault, "Check if task definiton \"%s\" exists ... \n", tmp);
    res = !SPIFFS.exists(&tmp[0]);
    wdtReset(dbgSetup);

    //infinite loop protection
    if (cnt > 200)
    {
      break;
    }
    cnt++;
  } // end - while, file exists

  if (res)
  {        
    sLen = strlen(fileName) + strlen(sIdx) + strlen(".def") + 1;
    (*aTsk).uid = str_setLength((*aTsk).uid, sLen);
    str_init((*aTsk).uid, sLen);
    strlcat((*aTsk).uid, &fileName[0], sLen);
    strlcat((*aTsk).uid, &sIdx[0], sLen);
    strlcat((*aTsk).uid, ".def", sLen);
    DBG_printf(dsDefault, "New task UID \"%s\" generated ... \n", (*aTsk).uid);    
  }
  else
  {
    DBG_printf(dsDefault, "Error during generate new task UID! \n");
  }
  wdtSoftEnable(dbgSetup);

  return res;
}

void srvHandle_TaskSet(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char msg[512] = "";
  char taskUid[50] = "";
  char argName[10] = "";
  t_task *tsk = NULL;
  t_task *paramTsk = NULL;
  uint8 i;
  uint8 idx;
  float fNum = 0;
  uint16 u16Num = 0;
  bool newTask = false;
  bool err = false;

  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "srvHandle_TaskSet() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  
  if (dbgSetup == dsDetails)
  {
    DBG_printf(dsDetails, "Number of arguments: %d \n", (*p_srv).args() - 1);
    for (i = 0; (i < (*p_srv).args() - 1) && (i < 255); i++)
    {
      DBG_printf(dsDetails, "\"%s\" = \"%s\" \n", (*p_srv).argName(i).c_str(), (*p_srv).arg(i).c_str());
      wdtReset(dbgSetup);
    }
  }

  err = false;
  str_init(&msg[0], sizeof(msg));

  //check all params
  if (
    (*p_srv).hasArg("task_uid")
    && (*p_srv).hasArg("task_active")
    && (*p_srv).hasArg("task_name")
    && (*p_srv).hasArg("task_desc")
    && (*p_srv).hasArg("task_img")
    && (*p_srv).hasArg("freq_start")
    && (*p_srv).hasArg("freq_stop")
    && (*p_srv).hasArg("task_pulse")
    && (*p_srv).hasArg("task_duration")
    && (*p_srv).hasArg("task_pause")    
    // ---
    && (*p_srv).hasArg("sched_0")
    && (*p_srv).hasArg("sched_1")
    && (*p_srv).hasArg("sched_2")
    && (*p_srv).hasArg("sched_3")
    && (*p_srv).hasArg("sched_4")
    && (*p_srv).hasArg("sched_5")
    && (*p_srv).hasArg("sched_6")
    && (*p_srv).hasArg("sched_7")
    && (*p_srv).hasArg("sched_8")
    && (*p_srv).hasArg("sched_9")
    && (*p_srv).hasArg("sched_10")
    && (*p_srv).hasArg("sched_11")
    && (*p_srv).hasArg("sched_12")
    && (*p_srv).hasArg("sched_13")
    && (*p_srv).hasArg("sched_14")
    && (*p_srv).hasArg("sched_15")
    && (*p_srv).hasArg("sched_16")
    && (*p_srv).hasArg("sched_17")
    && (*p_srv).hasArg("sched_18")
    && (*p_srv).hasArg("sched_19")
    && (*p_srv).hasArg("sched_20")
    && (*p_srv).hasArg("sched_21")
    && (*p_srv).hasArg("sched_22")
    && (*p_srv).hasArg("sched_23")                                
  )
  {
    //all ok - send data
    err = false;
  }
  else
  {
    strlcat(&msg[0], "Some of the task params are missing!", sizeof(msg));
    DBG_printf(dsDefault, "Some of the task params are missing! \n");
    err = true;
  }

  if (!err)
  {
    //read task params
    paramTsk = taskListSetSize(paramTsk, 0, 1);
    DBG_printf(dsDefault, "Read task params ... \n");

    if ((*p_srv).hasArg("task_uid"))
    {
      (*p_srv).arg("task_uid").toCharArray(&taskUid[0], sizeof(taskUid));
      str_sanitizeHttpParam(&taskUid[0]);
      DBG_printf(dsDefault, " task_uid=%s \n", taskUid);
    }   

    if ((*p_srv).hasArg("task_active"))
    {
      if (strcmp((*p_srv).arg("task_active").c_str(), "1") == 0)
      {
        (*paramTsk).status = tsEnabled;
      }
      else
      {
        (*paramTsk).status = tsDisabled;
      }
    }
    switch ((*paramTsk).status)
    {
    case tsEnabled:
      DBG_printf(dsDetails, " status = enabled \n");
      break;
    case tsDisabled:
      DBG_printf(dsDetails, " status = disabled \n");
      break;
    default:
      DBG_printf(dsDetails, " status = unknown \n");
      ;
      break;
    }
    wdtReset(dbgSetup);

    if ((*p_srv).hasArg("task_name"))
    {
      (*paramTsk).name = str_setLength((*paramTsk).name, (*p_srv).arg("task_name").length() + 1);
      strlcpy((*paramTsk).name, (*p_srv).arg("task_name").c_str(), (*p_srv).arg("task_name").length() + 1);
      str_sanitizeHttpParam((*paramTsk).name);
    }
    DBG_printf(dsDetails, " name = %s \n", (*paramTsk).name);
    wdtReset(dbgSetup);

    if ((*p_srv).hasArg("task_desc"))
    {
      (*paramTsk).description = str_setLength((*paramTsk).description, (*p_srv).arg("task_desc").length() + 1);
      strlcpy((*paramTsk).description, (*p_srv).arg("task_desc").c_str(), (*p_srv).arg("task_desc").length() + 1);
      str_sanitizeHttpParam((*paramTsk).description);
    }
    DBG_printf(dsDetails, " description = %s \n", (*paramTsk).description);
    wdtReset(dbgSetup);

    if ((*p_srv).hasArg("task_img"))
    {
      (*paramTsk).img = str_setLength((*paramTsk).img, (*p_srv).arg("task_img").length() + 1);
      strlcpy((*paramTsk).img, (*p_srv).arg("task_img").c_str(), (*p_srv).arg("task_img").length() + 1);
      str_sanitizeHttpParam((*paramTsk).img);
    }
    DBG_printf(dsDetails, " image = %s \n", (*paramTsk).img);
    wdtReset(dbgSetup);

    if ((*p_srv).hasArg("freq_start"))
    {
      fNum = 0;
      if (str2float(strdup((*p_srv).arg("freq_start").c_str()), &fNum))
      {
        (*paramTsk).freqStart = fNum;
      }
    }
    DBG_printf(dsDetails, " freq start = %.2f Hz\n", (*paramTsk).freqStart);
    wdtReset(dbgSetup);

    if ((*p_srv).hasArg("freq_stop"))
    {
      fNum = 0;
      if (str2float(strdup((*p_srv).arg("freq_stop").c_str()), &fNum))
      {
        (*paramTsk).freqStop = fNum;
      }
    }
    DBG_printf(dsDetails, " freq stop = %.2f Hz \n", (*paramTsk).freqStop);
    wdtReset(dbgSetup);

    if ((*p_srv).hasArg("task_pulse"))
    {
      if (str2uint8(strdup((*p_srv).arg("task_pulse").c_str()), &i))
      {
        (*paramTsk).freqPulse = i;
      }
    }
    DBG_printf(dsDetails, " pulsing = %d Hz \n", (*paramTsk).freqPulse);
    wdtReset(dbgSetup);

    if ((*p_srv).hasArg("task_duration"))
    {
      u16Num = 0;
      if (str2uint16(strdup((*p_srv).arg("task_duration").c_str()), &u16Num))
      {
        (*paramTsk).duration = u16Num;
      }
    }
    DBG_printf(dsDetails, " duration = %d s \n", (*paramTsk).duration);
    wdtReset(dbgSetup);

    if ((*p_srv).hasArg("task_pause"))
    {
      u16Num = 0;
      if (str2uint16(strdup((*p_srv).arg("task_pause").c_str()), &u16Num))
      {
        (*paramTsk).pause = u16Num;
      }
    }
    (*paramTsk).pause_counter = 0;  //reset pause counter
    DBG_printf(dsDetails, " pause = %d s \n", (*paramTsk).pause);
    wdtReset(dbgSetup);

    for (idx = 0; idx < 24; idx++)
    {
      str_init(&argName[0], sizeof(argName));
      snprintf(&argName[0], sizeof(argName), "sched_%d", idx);
      if ((*p_srv).hasArg(&argName[0]))
      {
        if (str2uint8(strdup((*p_srv).arg(&argName[0]).c_str()), &i))
        {
          setSchedule(&(*paramTsk).schedule, idx, i);
        }
        DBG_printf(dsDetails, " parameter found schedule_%d = %d \n", idx, i);
      }
      else
      {
        DBG_printf(dsDetails, " parametr not foundschedule %d = %d \n", idx, i);
      }
      wdtReset(dbgSetup);
    }


    //find task
    for (i = 0; i < global.task_cnt; i++)
    {
      if (strcmp(p_task_list[i].uid, taskUid) == 0)
      {
        tsk = &p_task_list[i];
        break;
      }
      wdtReset(dbgSetup);
    }
    if (tsk == NULL)
    {
      //create new task?
      if (global.task_cnt <= TASK_MAX_CNT)
      {
        DBG_printf(dsDefault, "Task \"%s\" not found - create new task ... \n", taskUid);
        newTask = true;

        if (!generateTaskUID(paramTsk))
        {
          strlcat(&msg[0], "Error during generating new task UID!", sizeof(msg));
          DBG_printf(dsDefault, "Error during generating new task UID! \n");
          err = true;
        }
      }
      else
      {
        strlcat(&msg[0], "Maximum task count reached!", sizeof(msg));
        DBG_printf(dsDefault, "Maximum task count (%d) reached! \n", TASK_MAX_CNT);
        err = true;
      }
    }
    else
    {
      DBG_printf(dsDefault, "Task \"%s\" found ... update values \n", (*tsk).name);

      //set UID      
      (*paramTsk).uid = str_setLength((*paramTsk).uid, strlen(taskUid) + 1);
      str_init((*paramTsk).uid, strlen(taskUid) + 1);
      strlcpy((*paramTsk).uid, &taskUid[0], strlen(taskUid) + 1);

      //set Idx
      (*paramTsk).idx = (*tsk).idx;

      //assign params
      assignTaskParams(paramTsk, tsk);
    }
    wdtReset(dbgSetup);

    // -------------------------------------------------------------------------------------
    if (!err)
    {
      if (newTask)
      {
        tsk = paramTsk;
      }

      //#1-2 - save task params to SPIFFS
      if (saveTask(tsk))
      {
        DBG_printf(dsDetails, "Task (%s) params sucessfully saved \n", (*tsk).uid);
        strlcat(&msg[0], "ok", sizeof(msg));

        if (newTask)
        {
          read_task_list(); //reload task list
        }

        if ((
          ((*tsk).status == tsEnabled)      //task status is enabled
          && (settings.vap_stat == vsStarted) //vaporizer is started
          && (global.task_active_idx == 0)  //there is no active task running
        )) 
        {
          tck.doTaskUpdate = true; //UpdateTask will be triggered
        }
        else if (global.task_active_idx == (*tsk).idx)  //i am changing running task
        {
          tck.doRestartRunningTask = true;
        }
      }
      else
      {
        err = true;
        DBG_printf(dsDetails, "Error during saving the task (%s)! \n", (*tsk).uid);
        strlcat(&msg[0], "Error during saving task!", sizeof(msg));
      }
    } //err - task not found
    // -------------------------------------------------------------------------------------    

    //free param trask    
    paramTsk = taskListSetSize(paramTsk, 1, 0);
  } // - all params defined
  

  //#4 - send response
  if (!err)
  {
    (*p_srv).send(200, "text/html", "");
    (*p_srv).sendContent(msg);
  }
  else
  {
    (*p_srv).send(400, "text/html", "");
    (*p_srv).sendContent(msg);
  }

  //#5 - finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();

  wdtSoftEnable(dbgSetup);
}

void srvHandle_WifiSet(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  uint8 i;
  char tmp[100] = "";
  bool doRestart = false;

  DBG_printf(dsDefault, "srvHandle_WifiSet() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "text/html", "");
  
  if (dbgSetup == dsDetails)
  {
    DBG_printf(dsDetails, "Number of arguments: %d \n", (*p_srv).args() - 1);
    for (i = 0; (i < (*p_srv).args() - 1) && (i < 255); i++)
    {
      DBG_printf(dsDetails, "\"%s\" = \"%s\" \n", (*p_srv).argName(i).c_str(), (*p_srv).arg(i).c_str());
      wdtReset(dbgSetup);
    }
  }

  //data
  if ((*p_srv).hasArg("wifi_mode"))
  {
    if (strcmp((*p_srv).arg("wifi_mode").c_str(), "client") == 0)
    {
      doRestart = (settings.mode != vmClient);
      settings.mode = vmClient;
    }
    else
    {
      doRestart = (settings.mode != vmAP);
      settings.mode = vmAP;
    }
  }
  else
  {
    if (settings.mode != vmAP)
    {
      settings.mode = vmAP;
      doRestart = true;
    }
  }

  if ((*p_srv).hasArg("ssid"))
  {
    str_init(&tmp[0], sizeof(tmp));
    strlcpy(&tmp[0], (*p_srv).arg("ssid").c_str(), sizeof(tmp));    
    str_sanitizeHttpParam(&tmp[0]);
    doRestart = doRestart || (strcmp(&settings.ssid[0], &tmp[0]) != 0);    
    str_init(&settings.ssid[0], sizeof(settings.ssid));
    strlcpy(&settings.ssid[0], &tmp[0], sizeof(settings.ssid));
  }
  else
  {
    if (strcmp(&settings.ssid[0], "Vaporizer 0.1") != 0)
    {
      str_init(&settings.ssid[0], sizeof(settings.ssid));
      strlcpy(&settings.ssid[0], "Vaporizer 0.1", sizeof(settings.ssid));
      doRestart = true;
    }
  }

  if ((*p_srv).hasArg("password"))
  {
    str_init(&tmp[0], sizeof(tmp));
    strlcpy(&tmp[0], (*p_srv).arg("password").c_str(), sizeof(tmp));
    str_sanitizeHttpParam(&tmp[0]);
    if (strlen(tmp) < 8) //wpa password must have at leas 8 chars ...
    {      
      str_init(&tmp[0], sizeof(tmp));
      strlcpy(&tmp[0], "", sizeof(tmp));
    }
    doRestart = doRestart || (strcmp(&settings.pass[0], &tmp[0]) != 0);
    str_init(&settings.pass[0], sizeof(settings.pass));
    strlcpy(&settings.pass[0], &tmp[0], sizeof(settings.pass));
  }
  else
  {
    if (strlen(settings.pass) > 0)
    {
      str_init(&settings.pass[0], sizeof(settings.pass));
      strlcpy(&settings.pass[0], "", sizeof(settings.pass));      
      doRestart = true;
    }
  }
  

  if ((*p_srv).hasArg("use_dhcp"))
  {
    if (strcmp((*p_srv).arg("use_dhcp").c_str(), "1") == 0)
    {
      doRestart = doRestart || (settings.use_dhcp == 0);
      settings.use_dhcp = 1;
    }
    else
    {
      doRestart = doRestart || (settings.use_dhcp == 1);
      settings.use_dhcp = 0;
    }
  }
  else
  {
    if (settings.use_dhcp == 1)
    {
      settings.use_dhcp = 0;
      doRestart = true;
    }
  }
  

  if ((*p_srv).hasArg("ip_address"))
  {
    str_init(&tmp[0], sizeof(tmp));
    strlcpy(&tmp[0], (*p_srv).arg("ip_address").c_str(), sizeof(tmp));
    str_sanitizeHttpParam(&tmp[0]);
    doRestart = doRestart || (strcmp(&settings.cli_ip[0], &tmp[0]) != 0);
    str_init(&settings.cli_ip[0], sizeof(settings.cli_ip));
    strlcpy(&settings.cli_ip[0], &tmp[0], sizeof(settings.cli_ip));
  }
  else
  {
    if (strlen(settings.cli_ip) > 0)
    {
      str_init(&settings.cli_ip[0], sizeof(settings.cli_ip));
      strlcpy(&settings.cli_ip[0], "", sizeof(settings.cli_ip));
      doRestart = true;
    }
  }

  if ((*p_srv).hasArg("ip_gateway"))
  {
    str_init(&tmp[0], sizeof(tmp));
    strlcpy(&tmp[0], (*p_srv).arg("ip_gateway").c_str(), sizeof(tmp));
    str_sanitizeHttpParam(&tmp[0]);
    doRestart = doRestart || (strcmp(&settings.cli_gateway[0], &tmp[0]) != 0);
    str_init(&settings.cli_gateway[0], sizeof(settings.cli_gateway));
    strlcpy(&settings.cli_gateway[0], &tmp[0], sizeof(settings.cli_gateway));
  }
  else
  {
    if (strlen(settings.cli_gateway) > 0)
    {
      str_init(&settings.cli_gateway[0], sizeof(settings.cli_gateway));
      strlcpy(&settings.cli_gateway[0], "", sizeof(settings.cli_gateway));
      doRestart = true;
    }
  }

  if ((*p_srv).hasArg("net_mask"))
  {
    str_init(&tmp[0], sizeof(tmp));
    strlcpy(&tmp[0], (*p_srv).arg("net_mask").c_str(), sizeof(tmp));
    str_sanitizeHttpParam(&tmp[0]);
    doRestart = doRestart || (strcmp(&settings.cli_subnet[0], &tmp[0]) != 0);
    str_init(&settings.cli_subnet[0], sizeof(settings.cli_subnet));
    strlcpy(&settings.cli_subnet[0], &tmp[0], sizeof(settings.cli_subnet));
  }
  else
  {
    if (strlen(settings.cli_subnet) > 0)
    {
      str_init(&settings.cli_subnet[0], sizeof(settings.cli_subnet));
      strlcpy(&settings.cli_subnet[0], "", sizeof(settings.cli_subnet));
      doRestart = true;
    }
  }

  if ((*p_srv).hasArg("ip_dns"))
  {
    str_init(&tmp[0], sizeof(tmp));
    strlcpy(&tmp[0], (*p_srv).arg("ip_dns").c_str(), sizeof(tmp));
    str_sanitizeHttpParam(&tmp[0]);
    doRestart = doRestart || (strcmp(&settings.cli_dns[0], &tmp[0]) != 0);                
    str_init(&settings.cli_dns[0], sizeof(settings.cli_dns));
    strlcpy(&settings.cli_dns[0], &tmp[0], sizeof(settings.cli_dns));
  }
  else
  {
    if (strlen(settings.cli_dns) > 0)
    {
      str_init(&settings.cli_dns[0], sizeof(settings.cli_dns));
      strlcpy(&settings.cli_dns[0], "", sizeof(settings.cli_dns));
      doRestart = true;
    }
  }
  
  write_eeprom_settings();
  (*p_srv).sendContent("ok");  

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop(); 

  if (doRestart) {
    DBG_printf(dsDefault, "Restart device ... \n");
    ESP.restart();    
  }
}

void srvHandle_SystemSet(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  bool isSummer;
  char str[11] = "";
  char sNum[5] = "";
  uint8 i;
  uint8 num8 = 0;
  uint16 num16 = 0;

  DBG_printf(dsDefault, "srvHandle_SystemSet() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);
  (*p_srv).send(200, "text/html", "");
  
  if (dbgSetup == dsDetails)
  {
    DBG_printf(dsDetails, "Number of arguments: %d \n", (*p_srv).args() - 1);
    for (i = 0; (i < (*p_srv).args() - 1) && (i < 255); i++)
    {
      DBG_printf(dsDetails, "\"%s\" = \"%s\" \n", (*p_srv).argName(i).c_str(), (*p_srv).arg(i).c_str());
      wdtReset(dbgSetup);
    }
  }

  //date - dd.mm.yyyy
  if ((*p_srv).hasArg("sys_date"))
  {
    strlcpy(&str[0], (*p_srv).arg("sys_date").c_str(), sizeof(str));
    DBG_printf(dsDetails, "sys_date=\"%s\" \n", str);

    if (strlen(str) == 10)
    {  
      str_init(&sNum[0], sizeof(sNum));
      strncpy(&sNum[0], &str[0], 4); //copy max 4 charaters
      if (str2uint16(&sNum[0], &num16))
      {
        global.dYear = num16;
      }

      str_init(&sNum[0], sizeof(sNum));
      strncpy(&sNum[0], &str[5], 2);
      if (str2uint8(&sNum[0], &num8))
      {
        global.dMonth = num8;
      }

      str_init(&sNum[0], sizeof(sNum));
      strncpy(&sNum[0], &str[8], 2);
      if (str2uint8(&sNum[0], &num8))
      {
        global.dDay = num8;
      }
    }
  }

  //time - hh:nn:ss
  if ((*p_srv).hasArg("sys_time"))
  {
    strlcpy(&str[0], (*p_srv).arg("sys_time").c_str(), sizeof(str));
    DBG_printf(dsDetails, "sys_time=\"%s\" \n", str);

    if (strlen(str) >= 5)
    {
      str_init(&sNum[0], sizeof(sNum));
      strncpy(&sNum[0], &str[0], 2);
      if (str2uint8(&sNum[0], &num8))
      {
        global.tHour = num8;
      }

      str_init(&sNum[0], sizeof(sNum));
      strncpy(&sNum[0], &str[3], 2);
      if (str2uint8(&sNum[0], &num8))
      {
        global.tMin = num8;
      }
    }

    //seconds must not being set
    if (strlen(str) == 8)
    {
      str_init(&sNum[0], sizeof(sNum));
      strncpy(&sNum[0], &str[6], 2);
      if (str2uint8(&sNum[0], &num8))
      {
        global.tSec = num8;
      }      
    }
    else
    {
      global.tSec = 0;      
    }
  }
  
  isSummer = isSummerTime(global.dYear, global.dMonth, global.dDay, global.tHour);
  if (settings.isSummerTime != isSummer)
  {
    settings.isSummerTime = isSummer;
    write_eeprom_settings();
  }

  write_ds3231_datetime_settings();
  (*p_srv).sendContent("ok");

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();
}

void srvHandle_SecuritySet(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  bool actUsrErr = false;
  bool actPassErr = false;
  bool newPassErr = false;
  char str[21] = "";
  char result[512] = "";

  DBG_printf(dsDefault, "srvHandle_SecuritySet() \n");

  //header
  (*p_srv).sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  (*p_srv).sendHeader("Pragma", "no-cache");
  (*p_srv).sendHeader("Expires", "-1");
  (*p_srv).setContentLength(CONTENT_LENGTH_UNKNOWN);

  if (dbgSetup == dsDetails)
  {
    uint8 i;
    DBG_printf(dsDetails, "Number of arguments: %d \n", (*p_srv).args() - 1);
    for (i = 0; (i < (*p_srv).args() - 1) && (i < 255); i++)
    {
      DBG_printf(dsDetails, "\"%s\" = \"%s\" \n", (*p_srv).argName(i).c_str(), (*p_srv).arg(i).c_str());
      wdtReset(dbgSetup);
    }
  }

  if (
    (*p_srv).hasArg("usr_name_act")
    && (*p_srv).hasArg("usr_pass_act")
    && (*p_srv).hasArg("usr_name_new")
    && (*p_srv).hasArg("usr_pass_new")
    && (*p_srv).hasArg("usr_pass_confirm")    
  )
  {    
    //actual user name
    str_init(&str[0], sizeof(str));
    strlcpy(&str[0], (*p_srv).arg("usr_name_act").c_str(), sizeof(str));
    str_sanitizeHttpParam(&str[0]);
    actUsrErr = (strcmp(&str[0], &settings.vapLogin[0]) != 0);
    if (actUsrErr)
    {
      DBG_printf(dsDefault, "Actual user name \"%s\" not match! \n", settings.vapLogin);
      str_init(result, sizeof(result));
      snprintf(&result[0], sizeof(result), "Actual user name \"%s\" not match! \n", settings.vapLogin);
    }
    //actual password
    str_init(&str[0], sizeof(str));
    strlcpy(&str[0], (*p_srv).arg("usr_pass_act").c_str(), sizeof(str));
    str_sanitizeHttpParam(&str[0]);
    actPassErr = (strcmp(&str[0], &settings.vapPass[0]) != 0);
    if (actPassErr)
    {
      DBG_printf(dsDefault, "Password \"%s\" not match actual password \"%s\"! \n", str, settings.vapPass);
      str_init(result, sizeof(result));
      snprintf(&result[0], sizeof(result), "Password \"%s\" not match actual password! \n", str);
    }
    // ---
    //new password    
    newPassErr = (strcmp((*p_srv).arg("usr_pass_new").c_str(), (*p_srv).arg("usr_pass_confirm").c_str()) != 0);
    if (newPassErr)
    {
      DBG_printf(dsDefault, "New password not match confirm password! \n");
      str_init(result, sizeof(result));
      snprintf(&result[0], sizeof(result), "New password not match confirm password! \n");
    }
         
    if (actUsrErr || actPassErr || newPassErr)
    {
      (*p_srv).send(400, "text/html", "");
      (*p_srv).sendContent(result);
    }
    else
    {
      (*p_srv).send(200, "text/html", "");
      (*p_srv).sendContent("Ok");

      //set new values
      str_init(&settings.vapLogin[0], sizeof(settings.vapLogin));
      strlcpy(&settings.vapLogin[0], (*p_srv).arg("usr_name_new").c_str(), sizeof(settings.vapLogin));
      str_sanitizeHttpParam(&settings.vapLogin[0]);
      //
      str_init(&settings.vapPass[0], sizeof(settings.vapPass));
      strlcpy(&settings.vapPass[0], (*p_srv).arg("usr_pass_new").c_str(), sizeof(settings.vapPass));
      str_sanitizeHttpParam(&settings.vapPass[0]);
      //
      //write to EEPROM
      write_eeprom_settings();
    }  
  }
  else
  {
    (*p_srv).send(400, "text/html", "");
    (*p_srv).sendContent("Invalid parameters!");
  }

  //finalize response
  (*p_srv).sendContent("");
  (*p_srv).client().stop();
}

void srvHandle_TaskDelete(void)
{
  if (!IsServerAutenticated()) { return DoServerAutentication(); }  //login check

  char taskUid[50] = "";
  char tmp[255] = "";

  DBG_printf(dsDefault, "srvHandle_TaskDelete() \n");

  //data
  //get task UID (task file name)
  if ((*p_srv).hasArg("task_uid"))
  {
    wdtSoftDisable(dbgSetup);
    (*p_srv).arg("task_uid").toCharArray(&taskUid[0], sizeof(taskUid));

    str_init(&tmp[0], sizeof(tmp));
    strlcat(&tmp[0], TASK_DEF_DIR, sizeof(tmp));
    strlcat(&tmp[0], taskUid, sizeof(tmp));

    if (SPIFFS.exists(tmp))
    {
      //pause vaporizer
      if (settings.vap_stat == vsStarted)
      {
        settings.vap_stat = vsPaused;
      }

      //delete file
      if (SPIFFS.remove(tmp))
      {
        DBG_printf(dsDefault, "Task \"%s\" deleted ... \n", taskUid);
      }
      else
      {
        DBG_printf(dsDefault, "Error during delete task \"%s\" ! \n", taskUid);
      }

      //restart vaporizer
      read_task_list();
      if (settings.vap_stat == vsPaused)
      {
        settings.vap_stat = vsStarted;
        tck.doTaskUpdate = true; //UpdateTask will be triggered
      }      
    }
    else 
    {
      DBG_printf(dsDefault, "Task \"%s\" not found! \n", tmp);
    }
    wdtSoftEnable(dbgSetup);
  }
  else
  {
    DBG_printf(dsDefault, "Task uid not set! \n");
  }  
  srvHandle_RedirectTo(strdup(HTML_TASK_LIST));     
}

void setup_AP(void)
{
  DBG_printf(dsDefault, "Setup AP ... \n");

  if (WiFi.status() != WL_DISCONNECTED)
  {
    WiFi.mode(WIFI_OFF); //disable wifi
    delay(100);
  }

  const char *p_ap_ssid = &settings.ssid[0];
  const char *p_ap_pass = &settings.pass[0];

  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(
    IPAddress(192, 168, 10, 1),
    IPAddress(192, 168, 10, 1),
    IPAddress(255, 255, 255, 0)
  );
  WiFi.softAP(p_ap_ssid, p_ap_pass);

  strlcpy(global.ip, WiFi.softAPIP().toString().c_str(), sizeof(global.ip));

  DBG_printf(dsDefault, "ssid: \"%s\" \n", p_ap_ssid);
  DBG_printf(dsDefault, "password: \"%s\" \n", p_ap_pass);
  DBG_printf(dsDefault, "IP address: \"%s\" \n", global.ip);

  DBG_printf(dsDefault, "AP started \n");
}

void setup_CLIENT(void)
{
  DBG_printf(dsDefault, "Setup CLIENT ... \n");

  if (WiFi.status() != WL_DISCONNECTED)
  {
    DBG_printf(dsDefault, "Disable WiFi ... \n");
    WiFi.mode(WIFI_OFF); //disable wifi
    delay(100);
  }

  const char *p_cli_ssid = &settings.ssid[0];
  const char *p_cli_pass = &settings.pass[0];

  IPAddress ip;
  IPAddress gateway;
  IPAddress subnet;
  IPAddress dns;

  DBG_printf(dsDefault, "Set STA mode ... \n");
  WiFi.mode(WIFI_STA);

  if (settings.use_dhcp == 0)
  {  
    ip.fromString(settings.cli_ip);
    gateway.fromString(settings.cli_gateway);
    subnet.fromString(settings.cli_subnet);
    dns.fromString(settings.cli_dns);

    WiFi.config(ip, gateway, subnet, dns);  //setup IP
  } 

  //connect to wifi
  DBG_printf(dsDefault, "Connecting to ssid: \"%s\", pass: \"%s\" \n", p_cli_ssid, p_cli_pass);
  WiFi.begin(p_cli_ssid, p_cli_pass);

  wdtSoftDisable(dbgSetup);
  int i = 0;  
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    wdtReset(dbgSetup);
    i++;
    DBG_printf(dsDefault, "%d ", i);
    
    if (i > 20) {
      DBG_printf(dsDefault, " ... CLIENT not connected!\n");
      DBG_printf(dsDefault, "Restart device ... \n");
      ESP.restart(); //restart vaporizer
      return; //for sure
    }
  }
  wdtSoftEnable(dbgSetup);

  if (WiFi.status() == WL_CONNECTED)
  {
    DBG_printf(dsDefault, " ... CLIENT connected - OK \n");
    str_init(&global.ip[0], sizeof(global.ip));
    strlcpy(&global.ip[0], WiFi.localIP().toString().c_str(), sizeof(global.ip));
    DBG_printf(dsDefault, " ip: \"%s\" \n", global.ip);
  }
}

void start_SERVER(void)
{
  DBG_printf(dsDefault, "Start server ... \n");

  p_srv = new ESP8266WebServer();

  //request handlers
  (*p_srv).onNotFound(srvHandle_NotFound);
  (*p_srv).on("/", srvHandle_Root);
  //
  (*p_srv).on("/api/start", srvHandle_Start);
  (*p_srv).on("/api/stop", srvHandle_Stop);
  //
  (*p_srv).on("/api/status/buttons", srvHandle_Buttons);
  (*p_srv).on("/api/status/voltage", srvHandle_Voltage);
  (*p_srv).on("/api/status/humidity", srvHandle_Humidity);
  (*p_srv).on("/api/status/temperature", srvHandle_Temperature);
  (*p_srv).on("/api/status/task", srvHandle_ActualTask);
  //
  (*p_srv).on("/api/task/list", srvHandle_TaskList);
  (*p_srv).on("/api/task/img/list", srvHandle_TaskImgList);
  (*p_srv).on(HTML_SETTINGS_TASK, srvHandle_TaskEdit);
  //
  (*p_srv).on("/api/settings/task/get", srvHandle_TaskGet);
  (*p_srv).on("/api/settings/wifi/get", srvHandle_WifiGet);
  (*p_srv).on("/api/settings/system/get", srvHandle_SystemGet);
  (*p_srv).on("/api/settings/security/get", srvHandle_SecurityGet);
  //
  (*p_srv).on("/api/settings/task/set", srvHandle_TaskSet);
  (*p_srv).on("/api/settings/wifi/set", srvHandle_WifiSet);
  (*p_srv).on("/api/settings/system/set", srvHandle_SystemSet);
  (*p_srv).on("/api/settings/security/set", srvHandle_SecuritySet);
  //
  (*p_srv).on("/api/settings/task/delete", srvHandle_TaskDelete);

  //start server
  (*p_srv).begin();
  DBG_printf(dsDefault, "Server started \n");
}

void stop_SERVER(void)
{
  DBG_printf(dsDefault, "Stop server ... \n");
  if (p_srv != NULL)
  {
    (*p_srv).stop(); //stop server
    delete p_srv;
    p_srv = NULL;
  }
  DBG_printf(dsDefault, "Server stopped \n");  
}

void stop_WIFI(void)
{
  DBG_printf(dsDefault, "Stop WiFi ... \n");
  WiFi.mode(WIFI_OFF); //wifi off
  DBG_printf(dsDefault, "WiFi stopped \n");
}

bool read_ntp_datetime(bool aUpdateRTC)
{
  bool res = false;
  bool isSummer;

  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "read_ntp_datetime() ... \n");
  if (settings.mode == vmClient) {
    if (WiFi.status() == WL_CONNECTED)
    {
      char ntpSrv[100] = "cz.pool.ntp.org";

      DBG_printf(dsDefault, "Create UDP socket ... ");
      p_udp = new WiFiUDP();
      DBG_printf(dsDefault, "OK \n");
      DBG_printf(dsDefault, "Create NTP client for \"%s\"... ", ntpSrv);
      p_ntp = new NTPClient(p_udp, ntpSrv);
      DBG_printf(dsDefault, "OK \n");
      wdtReset(dbgSetup);

      // ---
      (*p_ntp).begin();
      res = (*p_ntp).forceUpdate();
      (*p_ntp).setTimeOffset(3600); //GMT+1, Prague

      if (res)
      {
        isSummer = isSummerTime( (*p_ntp).getYear(), (*p_ntp).getMonth(), (*p_ntp).getDay(), (*p_ntp).getHours() );        
        if (isSummer)
        {
          //add 1hour shift
          (*p_ntp).setTimeOffset(3600+3600); //GMT+1, Prague + 1hour shift --> "summer time"
        }
        if (isSummer != settings.isSummerTime)
        {
          settings.isSummerTime = isSummer;
          wdtReset(dbgSetup);
          write_eeprom_settings(); //write summer time to EEPROm
        }

        global.dYear = (*p_ntp).getYear();
        global.dMonth = (*p_ntp).getMonth();
        global.dDay = (*p_ntp).getDay();
        global.tHour = (*p_ntp).getHours();
        global.tMin = (*p_ntp).getMinutes();
        global.tSec = (*p_ntp).getSeconds();        

        DBG_printf(dsDefault, "Readed date time \"%02d.%02d.%04d - %02d:%02d:%02d\", summer=%d, (%u)... \n",
          global.dDay, global.dMonth, global.dYear, global.tHour, global.tMin, global.tSec, settings.isSummerTime, (*p_ntp).getUnixTime()
        );

        if (aUpdateRTC)
        {
          wdtReset(dbgSetup);
          write_ds3231_datetime_settings(); //write date time to RTC
        }
      }
      else
      {
        DBG_printf(dsDefault, "Error while reading NTP server response! \n");  
      }
      // ---

      wdtReset(dbgSetup);
      DBG_printf(dsDefault, "Destroy NTP client ... ");
      (*p_ntp).end();
      delete p_ntp;
      DBG_printf(dsDefault, "OK \n");
      DBG_printf(dsDefault, "Destroy UPD socket ... ");
      delete p_udp;      
      DBG_printf(dsDefault, "OK \n");    
      p_ntp = NULL;
      p_udp = NULL;
    }
    else
    {
      DBG_printf(dsDefault, "Device is not connected to the WiFi network! \n");
    }
  }
  else
  {
    DBG_printf(dsDefault, "Device is not in the CLIENT mode! \n");
  }
  wdtSoftEnable(dbgSetup);

  return res;
}

void UpdateSystem(void)
{
  bool isSummer;

  //# update time (1Hz)
  if (tck.doSystem)
  {
    tck.doSystem = false;

    //active task duration
    if (global.task_active_idx > 0)
    {
      global.task_active_duration++;
    }

    //paused task duration
    if (global.task_cnt > 0)
    {
      uint8 i;
      for (i=0; i < global.task_cnt; i++)
      {
        if (p_task_list[i].pause_counter > 0)
        {                    
          p_task_list[i].pause_counter--;
        }
      }
    }

    //system date time
    global.tSec++;
    if (global.tSec > 59)
    {
      global.tSec = global.tSec % 60;
      global.tMin++;
    }
    if (global.tMin > 59)
    {
      global.tMin = global.tMin % 60;
      global.tHour++;

      if ((global.dMonth == 3) || (global.dMonth == 10))
      {
        isSummer = isSummerTime(global.dYear, global.dMonth, global.dDay, global.tHour);
        if (settings.isSummerTime != isSummer)
        {
          if (isSummer)
          {
            global.tHour++; //shift by 1 hour forward
          }
          else
          {
            global.tHour--; //shift by 1 hour backwards
          }
          settings.isSummerTime = isSummer;

          write_ds3231_datetime_settings();          
          write_eeprom_settings();
        }
      }

    }    
    if (global.tHour > 23)
    {
      //once per day, read new date from NTP server or RCT
      global.tHour = 0;        
      if (!read_ntp_datetime(true)) //read datetime from NTP server and update RTC settings
      {
        read_ds3231_datetime(); //end of day, read new date from RTC        
      }
    }

    //DBG_printf(dsDetails, "Heap %d B RAM ... \n", ESP.getFreeHeap());
  }
}

void UpdateStatus(void)
{
  //# update status indicators (temperature, humidity battery)
  if (tck.doStatusUpdate)
  {
    tck.doStatusUpdate = false;    
    read_status();
  }
}

void UpdateActiveTaskFreq(void)
{
  if (global.task_active_idx > 0)
  {
    if (global.task_active_freq_step > 0)
    {
      global.task_active_freq = global.task_active_freq + global.task_active_freq_step;
      set_ad9833_freq(global.task_active_freq, false);
    }
  }
}

void UpdateTask(void)
{
  t_task *p_tskActive = NULL;
  t_task *p_tskNext = NULL;
  bool TaskInProgress = false;
  bool TaskSchedulled = false;

  if (tck.doRestartRunningTask)
  {
    tck.doRestartRunningTask = false;
    p_tskActive = GetActiveTask();
    if (p_tskActive != NULL)
    {
      TaskSchedulled = IsTaskScheduled(p_tskActive); //task still in scheduller scope
      if (TaskSchedulled)
      {
        SetActiveTask(p_tskActive); //restart task
      }
      else
      { 
        ClearActiveTask(); //task not in scope - stop it
        set_ad9833_freq(0, false); //turn off generator
        setVapPulse(1); //mute output        
      }
      return; 
    }
  }

  if (tck.doTaskUpdate)
  {
    tck.doTaskUpdate = false;

    if (settings.vap_stat == vsStarted)
    {
      p_tskActive = GetActiveTask();
      if (p_tskActive != NULL)
      {
        TaskInProgress = IsActiveTaskInProgress(); //is actual task still in scope
        TaskSchedulled = IsTaskScheduled(p_tskActive); //task still in scheduller scope
    
        if (TaskInProgress && TaskSchedulled)
        {
          //update active task
          UpdateActiveTaskFreq();
        }
        else
        {          
          //set pause timeout
          (*p_tskActive).pause_counter = (*p_tskActive).pause;
          if ((*p_tskActive).pause_counter > 0)
          {
            DBG_printf(dsDefault, "Set task \"%s\" pause to %u second ... \n",  (*p_tskActive).name,  (*p_tskActive).pause_counter);
          }

          //select next enabled task
          p_tskNext = GetNextTask(p_tskActive);
        }
      }
      else
      {
        //select first enabled task
        p_tskNext = GetNextTask(NULL);
      }
      
      if (p_tskNext != NULL)
      {        
        SetActiveTask(p_tskNext); //start new task
      }
      else if (
          (p_tskActive != NULL) //some task is running
          && (!TaskInProgress || !TaskSchedulled) //and duration exceed or schedulle is out of scope
        )
      {
        //stop expired task
        ClearActiveTask();
        set_ad9833_freq(0, false); //turn off generator
        setVapPulse(1); //mute output
        setAmpTurnOff();
      }
    }
    else if (settings.vap_stat == vsStopped)
    {
      if (global.task_active_idx > 0)
      {
        ClearActiveTask();
        set_ad9833_freq(0, false); //turn off generator
        setVapPulse(1); //mute output
        setAmpTurnOff();
      }
    }        
  } //end - doTaskUpdate
}

void UpdatePulse()
{
  if (tck.doPulse)
  {
    tck.doPulse = false;
    toggleVapPulse();
  } //end - doPulse
}

// ----------------------------------------------------------------------------------
void setup()
{
  init_uart();
  init_FS();
  init_I2C();  
  init_vap_pulseIO(OUTPUT, HIGH);
  init_amp_IO();
  init_dht22();
  init_ad9833();
  init_global();

  //settings reset ?
  bool useDefaultSettings = false;  
  //
  wdtSoftDisable(dbgSetup);
  DBG_printf(dsDefault, "Reset settings? ... ");
  setVapPulse(0); //LED on
  delay(1000);
  wdtReset(dbgSetup);
  delay(1000);
  wdtReset(dbgSetup);
  delay(1000);
  wdtReset(dbgSetup);
  setVapPulse(1); //LED off
  
  init_vap_pulseIO(INPUT, HIGH);  
  useDefaultSettings = (digitalRead(PIN_VAP_PULSE) == 0); //button pressed ?
  init_vap_pulseIO(OUTPUT, HIGH);
  if (useDefaultSettings)    
  {
    DBG_printf(dsDefault, "YES \n");
  }
  else
  {
    DBG_printf(dsDefault, "NO \n");
  }
  wdtSoftEnable(dbgSetup);  
  //
  read_eeprom_settings(useDefaultSettings);  //wifi settings  + vaporizer state + sommer/winter time  
  
  read_task_list();
  read_status();

    //start wifi
  if (settings.mode == vmAP)
  {
    setup_AP();
  }
  else if (settings.mode == vmClient)
  {
    setup_CLIENT();  
  }

  if (!read_ntp_datetime(true)) //read and update RTC settings
  {
    read_ds3231_datetime();
    bool isSummer = isSummerTime(global.dYear, global.dMonth, global.dDay, global.tHour);
    if (settings.isSummerTime != isSummer)
    {
      if (isSummer)
      {
        global.tHour++; //add 1 hour shift
      }
      else
      {
        global.tHour--; //remove 1 hour shift
      }
      settings.isSummerTime = isSummer;

      write_ds3231_datetime_settings();
      write_eeprom_settings();
    }
  }

  start_SERVER();
  start_system_ticker();

  //update vaporizer state
  tck.doTaskUpdate = true; //start    
}

// ----------------------------------------------------------------------------------
void loop()
{
  (*p_srv).handleClient();  //wifi
  
  UpdateSystem(); //date time, actual task duration, pause duration  
  UpdateTask(); //task
  UpdatePulse(); //pulse
  UpdateStatus(); //temperature, voltage, humidity    
}
// ----------------------------------------------------------------------------------