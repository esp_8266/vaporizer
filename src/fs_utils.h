#ifndef _FS_UTILS_H
#define _FS_UTILS_H

#include <FS.h>
#include "vap_global.h"

//Hint: maximum SPIFFS file name lengt is 32 Bytes (31 chars + \0) !!!

String getContentType(String filename);
size_t FS_ReadUntil(File *f, char *row, size_t rowMaxByteLength, const char *strTerminator, t_dbg_setup dbgSetup);
size_t FS_ReadLine_CRLF(File *f, char *row, size_t rowMaxByteLength, t_dbg_setup dbgSetup);

#endif