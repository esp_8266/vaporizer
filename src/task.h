#ifndef _TASK_H
#define _TASK_H

#ifdef __cplusplus
extern "C"
{
#endif

#define TASK_MAX_CNT 50

//#include "vap_global.h"
#include "str_utils.h"

  typedef enum
  {
    tsUnknown,
    tsEnabled,
    tsDisabled    
  } t_task_stat;

  typedef struct
  {
    uint8 idx; //index in task array
    //
    char *uid;
    t_task_stat status;
    char *name;
    char *description;
    char *img;
    uint16 duration; //s
    uint16 pause; //s
    uint16 pause_counter; //s .. form pause counting
    float freqStart; //Hz
    float freqStop;  //Hz
    uint8 freqPulse; //Hz
    uint32 schedule; //bit 0 - 23,
  } t_task;

  t_task *taskListSetSize(t_task *tsk, uint8 actSize, uint8 newSize);
  void initTask(t_task *task);
  void finalizeTask(t_task *tsk);
  void assignTaskParams(t_task *tskSrc, t_task *tskDsc);
  bool TaskIsPaused(t_task *tsk);

  void setSchedule(uint32 *sched, uint8 idx, uint8 val);
  

#ifdef __cplusplus
}
#endif

#endif