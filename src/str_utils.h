#ifndef _STR_UTILS_H
#define _STR_UTILS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <Arduino.h>

  void str_init(char *str, size_t strByteLength);
  char *str_setLength(char *str, uint16 strByteLength);
  bool str_replace(char *str, size_t strMaxByteSize, const char *strPatern, const char *strReplace);
  int32 str_pos(const char *str, const char *strPatern);
  void str_replace_all(char *str, size_t strMaxByteSize, const char *strPatern, const char *strReplace);  
  void str_sanitizeHttpParam(char *str);
  void str_sanitizeFileName(char *str);

  //bool charIsNumber(char *strNum);

  bool str2uint8(char *str, uint8 *num);
  bool str2uint16(char *str, uint16 *num);
  bool str2uint32(char *str, uint32 *num);
  bool str2float(char *str, float *num);

  bool uint16_2str(uint16 *num, char *str, uint16 strMaxByteSize);
  bool uint8_2str(uint8 *num, char *str, uint16 strMaxByteSize);

  bool float2strEx(float *num, char *str, uint8 decimalCount, size_t strMaxByteSize);
  bool float2str(float *num, char *str, size_t strMaxByteSize);

#ifdef __cplusplus
}
#endif

#endif