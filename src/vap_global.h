#ifndef _VAP_GLOBAL_H
#define _VAP_GLOBAL_H

#ifdef __cplusplus
extern "C" {
#endif


#define DBG_OUTPUT_PORT Serial

//#define UART_SPEED 9600
#define UART_SPEED 19200
//#define UART_SPEED 38400
//#define UART_SPEED 57600
//#define UART_SPEED 115200

#define PIN_DHT22 2 //GPIO2 (D4), pins_arduino.h
#define PIN_VAP_PULSE 16 //GPIO16 (D0), pins_arduino.h (LED)
#define PIN_AMP_CONTROL 0 //GPIO0 (D3), FLASH

#define TICKER_MS_TIME_100 100
#define TICKER_MS_TIME_50 50
#define TICKER_MS_TIME_25 25

#define EEPROM_SIZE 512

#define TASK_DEF_DIR "/def/"

#define HTML_ROOT "/index.html"
#define HTML_SETTINGS_TASK "/settings_task.html"
#define HTML_TASK_LIST "/tasklist.html"

#define IMG_DIR "/img/"
#define IMG_STAT_ENABLED "stat-enabled.svg"
#define IMG_STAT_DISABLED "stat-disabled.svg"
#define IMG_STAT_IN_PROGRESS "stat-in-progress.svg"

#define IMG_DIR_TASK "/img/task/"

#include <Arduino.h>


typedef enum
{
  vsUnknown,
  vsStopped,
  vsStarted,
  vsPaused
} t_vap_stat;

typedef enum
{
  vmUnknown,
  vmClient,
  vmAP
} t_vap_mode;

typedef struct
{
  float temperature;
  float humidity;
  float battery_voltage;
  uint8 task_cnt;
  uint8 task_active_idx;
  float task_active_freq;
  float task_active_freq_step;
  float task_active_pulse_thr;
  uint16 task_active_duration;
  // ---
  char ip[16];
  // ---
  uint8 tHour;
  uint8 tMin;
  uint8 tSec;
  uint8 dDay;
  uint8 dMonth;
  uint16 dYear;
} t_global;

typedef struct
{
  uint16 ticCntSystem;  
  uint16 ticCntPulse;
  uint16 ticCntStatus;
  uint16 ticCntTaskUpdate;
  //
  bool doSystem;
  bool doPulse;  
  bool doStatusUpdate;  
  bool doTaskUpdate;  
  bool doRestartRunningTask;
} t_system_ticker;


typedef struct
{
  uint8 in_eeprom;      //1
  // ---
  char vapLogin[20];    //20  
  char vapPass[20];     //20
  // ---
  t_vap_mode mode;      //2 ?
  t_vap_stat vap_stat;  //2 ?
  char ssid[32];        //32
  char pass[64];        //64
  // ---
  uint8 use_dhcp;       //1
  char cli_ip[16];      //16
  char cli_gateway[16]; //16
  char cli_subnet[16];  //16
  char cli_dns[16];     //16
  // ---
  bool isSummerTime;    //1
} t_settings;

typedef enum
{
  dsNone,
  dsMinimal,
  dsDefault,
  dsDetails,
  dsUltimateDetails
} t_dbg_setup;


extern bool wdt_SoftActive;  //software watch dog status


#ifdef __cplusplus
}
#endif

#endif