#ifndef _WDT_UTILS_H
#define _WDT_UTILS_H

// software WDT tiemout = 3.2 seconds
// hardware WDT timeout = cca 7-8 seconds

#include "vap_global.h"

void wdtSoftDisable(t_dbg_setup dbgSetup);
void wdtSoftEnable(t_dbg_setup dbgSetup);
void wdtReset(t_dbg_setup dbgSetup);

#endif